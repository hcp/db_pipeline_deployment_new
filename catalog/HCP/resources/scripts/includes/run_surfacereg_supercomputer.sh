#!/bin/bash

############################################################################################################
## IMPORTANT NOTE:  This script assumes files from freesurfer run are still in place on the supercomputer ##
############################################################################################################

echo PATH=$PATH

maxhours=2

scriptdir=`dirname $0`
scriptdir=`readlink -f $scriptdir`

server=$1
user=$2
infile=$3
subject=$4
workdir=$5

filename=`echo $infile | sed -s 's/^.*\///'`

##
## SEE IF PASSWORD IS REQUIRED FOR HOST
##

ssh -o "BatchMode yes" $user@$server whoami

rc=$?
if [ $rc -ne 0 ] ; then
   echo "ERROR:  Cannot SSH to remote machine without password"
   exit $rc
fi

##
## COPY OVER SURFACEREG SCRIPT
##

scp $scriptdir/FreeSurfer2CaretConvertAndRegisterClean.sh $user@$server:~/pipeline_runs/$subject/script

rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not copy input file to remote server"
	exit $rc
fi

##
## CREATE DIRECTORIES ON REMOTE SERVER, GIVE EXECUTE PERMISSIONS ON SurfaceReg SCRIPT
##

ssh $user@$server "mkdir -p /scratch/$user/$subject/SurfaceReg"

rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not create directory on remote server"
	exit $rc
fi

##
## CREATE SurfaceReg SCRIPT FILE LOCALLY TO BE SENT TO REMOTE SERVER
##

#read -d '' FS_SCRIPT <<EOF
fsfile=`mktemp`
cat > $fsfile <<EOF
#!/bin/bash

# give the job a name to help keep track of running jobs (optional)
#PBS -N sreg_$subject

# Specify the resources needed
# We'll ask for 1 core of 1 node for 8 hours
## PBS -l nodes=1:ppn=1:idataplex,walltime=00:03:00
#PBS -l nodes=1:ppn=1:idataplex,walltime=02:00:00

# Specify the default queue, not the SMP nodes
#PBS -q dque

## supercomputer setup
export FREESURFER_HOME=/export/freesurfer-5.1
export PATH=\${FREESURFER_HOME}/bin:\${PATH}
export PATH=\${FREESURFER_HOME}/mni/bin:\${PATH}
export PERL5LIB=\${FREESURFER_HOME}/mni/lib/perl5/5.8.5/
export FSLDIR=/export/fsl-4.1.6
. \${FSLDIR}/etc/fslconf/fsl.sh
export PATH=\${FSLDIR}/bin:\${PATH}
export PATH=/export/caret/5.62/caret/bin_linux_intel64:\$PATH
export PATH=~/aff_conv:\$PATH

## pipeline64.local setup
#export FREESURFER_HOME=/usr/local/freesurfer
#export PATH=\$FREESURFER_HOME/bin:\$PATH
#export PATH=\$FREESURFER_HOME/mni/bin:\$PATH
#source \$FREESURFER_HOME/SetUpFreeSurfer.sh
#export FSLDIR=/usr/share/fsl
#. \${FSLDIR}/etc/fslconf/fsl.sh
#export PATH=\${FSLDIR}/bin:\${PATH}
#export PATH=/usr/local/caret/bin_linux64:\$PATH

time ~/pipeline_runs/$subject/script/FreeSurfer2CaretConvertAndRegisterClean.sh $subject /scratch/$user/$subject/SurfaceReg/caret /scratch/$user/$subject/SurfaceReg/data /scratch/$user/$subject/FreeSurfer/$subject ~/pipeline_runs/$subject/input/$filename 

EOF

rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Couldn't create surfacereg script file"
	exit $rc
fi

##
## SEND SurfaceReg SCRIPT FILE 
##

scp $fsfile $user@$server:~/pipeline_runs/$subject/script/sreg_job.sh

rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not send SurfaceReg script to remote server"
	exit $rc
fi

##
## CLEAN-UP
##

rm $fsfile

##
## RUN SurfaceReg PROCESS ON REMOTE SERVER
##

## RUN LOCALLY
#pid=`ssh $user@$server "chmod u+x ~/pipeline_runs/$subject/script/sreg_job.sh;nohup ~/pipeline_runs/$subject/script/sreg_job.sh > ~/pipeline_runs/$subject/script/launch_sreg_job.out 2> ~/pipeline_runs/$subject/script/launch_sreg_job.err < /dev/null & echo \\$!"` 
#echo PID=$pid
#rc=$?
#echo RC=$rc
### RUN ON SUPERCOMPUTER 
JOBID=`ssh $user@$server "if [ -f ~/pipeline_runs/$subject/input/$filename ]  && [ ${#user} -gt 0 ] && [ ${#subject} -gt 0 ] ; then qsub ~/pipeline_runs/$subject/script/sreg_job.sh ; else exit 1 ; fi"` 

rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not submit remote job"
	exit $rc
fi

echo "JOBID=$JOBID"

###
### Check status according to sleepsec seconds, continue when job is complete ##
###
#
iter=0
hour=0
sleepsec=300
#sleepsec=30

while [ $hour -lt $maxhours ] ; do
   sleep $sleepsec
   hour=$(echo "$iter*$sleepsec/3600" | bc)
   locheck=`ssh $user@$server 'rmcheck=\`qstat -f '$JOBID' 2> /dev/null < /dev/null\`; if [ $? -ne 0 ] ; then echo C ; else echo "$rmcheck" | grep -i "job_state" | sed s/^.*=\ *//; fi'`
   echo "job status=$locheck"
   if [ $locheck == C ] ; then
      break
   fi
   let iter+=1 
done

if [ $hour == $maxhours ] ; then
	echo "ERROR:  Timed out waiting for SurfaceReg run to complete ($maxhours hours)"
	exit 1;
fi

##
## Retrieve Files
##
## 1) zip up output on remote server
ssh $user@$server "cd /scratch/$user/$subject/SurfaceReg;zip -r ../surfacereg.zip ." 
rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not zip up SurfaceReg output"
	exit $rc
fi
## 2) retrieve zip file
cd $workdir
scp $user@$server:/scratch/$user/$subject/surfacereg.zip .
rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not retrieve SurfaceReg output from remote server"
	exit $rc
fi
## 3) unzip the file
unzip surfacereg.zip 
rm surfacereg.zip

##
## CLEAN-UP DIRECTORIES ON SERVER
##

ssh $user@$server "if [ -d ~/pipeline_runs/$subject ] && [ -O ~/pipeline_runs/$subject ] && [ -d /scratch/$user/$subject ] && [ -O /scratch/$user/$subject ]  && [ ${#user} -gt 0 ] && [ ${#subject} -gt 0 ] ; then  rm -r ~/pipeline_runs/$subject;rm -r /scratch/$user/$subject ; fi" 

echo $?


