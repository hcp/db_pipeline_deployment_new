#!/usr/bin/python

try:
        import json
except ImportError:
        import simplejson as json
import os
import shlex
import sys
import subprocess as sp
import zipfile
import logging

## If fewer than required arguments, just print version number
if (len(sys.argv)<11):
	print ("1.0")
	sys.exit(0)

## Assign variables
url=sys.argv[1]
user=sys.argv[2]
pw=sys.argv[3]
workdir=sys.argv[4]

projId=sys.argv[5]
subjId=sys.argv[6]
expId=sys.argv[7]
expLbl=sys.argv[8]

dtStamp=sys.argv[9]
statsxml=sys.argv[10]
fsloc=sys.argv[11]

try:
	currworkdir = workdir + '/' + expLbl + '/' 
	os.mkdir(workdir + '/' + expLbl + '/') 
except OSError:
	print('\n\nWARNING  Working directory already exists')

## Add separator if one's not there
if not url.endswith('/'):
   url+='/'

## Set up logging
logf = workdir + '/HCPStoreFS.log'
logging.basicConfig(filename=logf,level=logging.INFO,filemode='w')

## Check that various files, paths exist.  Exit if they don't
if (not os.path.exists(statsxml)):
	logging.error('\n\nERROR:  Statistics XML file, ' + statsxml + ' does not exist.  Exiting program.')
	sys.exit(1)
if (not os.path.exists(fsloc)):
	logging.error('\n\nERROR:  Freesurfer output location, ' + fsloc + ' does not exist.  Exiting program.')
	sys.exit(1)

## Create Assessor for statistics, uploading stats xml
sprc = sp.call(['curl','-u',user + ':' + pw,'-X','POST','-T',statsxml,url +  'REST/projects/' + projId + '/subjects/' + subjId + '/experiments/' + expId + '/assessors'])
if sprc!=0:
	logging.error('\n\nERROR:  Statistics could not be successfully generated from Freesurfer output')
	sys.exit(1)

## Zip up FS snapshots output and add as resource files
os.chdir(fsloc + '/snapshots')
zrc = sp.call(['zip','-r',currworkdir + '/snapshots.zip','.'])
## Zip up FS snapshots output and add as resource files
os.chdir(fsloc)
## Note how quotes in the command are handled here.  Don't think this is the best way, but it's working here.  Subprocess 
## seems to have trouble handling quotes in the command line
zrc = sp.call(['zip','-r',currworkdir + '/freesurfer.zip','.','-x','"','snapshots/*','"'])
os.chdir(currworkdir)

## Upload files as resources under assessor
## Using inbody transfers here.  Subprocess had trouble handling quotes required when using -F option of curl
sprc = sp.call(['curl','-u',user + ':' + pw,'-X','PUT','-T',currworkdir + 'freesurfer.zip',url +  'REST/projects/' + projId + '/subjects/' + subjId + '/experiments/' + expId + '/assessors/' + expId + '_freesurfer_' + dtStamp + '/out/resources/DATA/files/DATA?extract=true&inbody=true'])
if sprc!=0:
	logging.error('\n\nERROR:  Problem uploading freesurfer output')

sprc = sp.call(['curl','-u',user + ':' + pw,'-X','PUT','-T',currworkdir + 'snapshots.zip',url +  'REST/projects/' + projId + '/subjects/' + subjId + '/experiments/' + expId + '/assessors/' + expId + '_freesurfer_' + dtStamp + '/out/resources/SNAPSHOTS/files/SNAPSHOTS?extract=true&inbody=true'])
if sprc!=0:
	logging.error('\n\nERROR:  Problem uploading freesurfer output')

## Clean-up
os.remove('freesurfer.zip')
os.remove('snapshots.zip')


