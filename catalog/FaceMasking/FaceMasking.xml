<?xml version="1.0" encoding="UTF-8"?>
<Pipeline xmlns="http://nrg.wustl.edu/pipeline" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://nrg.wustl.edu/pipeline ..\schema\pipeline.xsd"  xmlns:fileUtils="http://www.xnat.org/java/org.nrg.imagingtools.utils.FileUtils">
	<name>FaceMasking</name>
	<!--Should be  Name of the pipeline XML file -->
	<location>/data/intradb/pipeline/catalog/FaceMasking</location>
	<!-- Filesystem path to the pipeline XML -->
	<description>The pipeline creates the face masked DICOM from the high resolution MRI head DICOM scans</description>
        <resourceRequirements>
                   <!-- NOTE:  Queues defined in /nrgpackages/sge_root/nrg/common/qtask.  Dev machines have overlay at /opt/sge_common/[DEV_MACH_NAME]/qtask which is mounted as a bind  -->
           <property name="DRMAA_JobTemplate_JobCategory">nrglab_q</property>
        </resourceRequirements>
	<documentation>
	   <authors>
	   	<author>
	   		<lastname>Milchenko</lastname>
			<firstname>Mikhail</firstname>
	   	</author>
	   </authors>
		<version>1</version>
		<input-parameters>
			<parameter>
				<name>scanids</name>
				<values><schemalink>xnat:mrSessionData/scans/scan/ID</schemalink></values>
				<description>The scan ids of all the scans of the session</description>
			</parameter>
			<parameter>
				<name>xnat_id</name>
				<values><schemalink>xnat:mrSessionData/ID</schemalink></values>
				<description>The scan ids of all the scans of the session</description>
			</parameter>
			<parameter>
				<name>sessionId</name>
				<values><schemalink>xnat:mrSessionData/label</schemalink></values>
				<description>The scan ids of all the scans of the session</description>
			</parameter>
			<parameter>
				<name>project</name>
				<values><schemalink>xnat:mrSessionData/project</schemalink></values>
				<description>Project</description>
			</parameter>
			<parameter>
				<name>subject</name>
				<values><schemalink>xnat:mrSessionData/subject_ID</schemalink></values>
				<description>Subject ID</description>
			</parameter>
			<parameter>
				<name>usebet</name>
				<values><csv>1</csv></values>
				<description>Set to 1 to generate an exclusion brain mask, 0 to skip this step.</description>
			</parameter>
			<parameter>
				<name>invasiveness</name>
				<values><csv>1.0</csv></values>
				<description>Invasiveness coefficient. Decrease/increase between 0.5 and 1.5 to control the degree of masking and depth of voxel alteration</description>
			</parameter>
			<parameter>
				<name>existing</name>
				<values><csv>Fail</csv></values>
				<description>What to do on existing scans (Fail, Skip or Overwrite)</description>
			</parameter>
			<parameter>
				<name>runOtherPipelines</name>
				<values><csv>Y</csv></values>
				<description>Launch subsequent pipelines?  (Y/N)</description>
			</parameter>
		</input-parameters>
	</documentation>
	<xnatInfo appliesTo="xnat:mrSessionData"/>
	<outputFileNamePrefix>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/maskface')^</outputFileNamePrefix>
	<loop id="series" xpath="^/Pipeline/parameters/parameter[name='scanids']/values/list^"/>
	<!-- Description of the Pipeilne -->
	<parameters>
		<parameter>
			<name>mailhost</name>
			<values>
			  <unique>artsci.wustl.edu</unique>
			  </values>
		</parameter>
		<parameter>
			<name>workdir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='builddir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='sessionId']/values/unique/text())^</unique>
			</values>
		</parameter>
	</parameters>
	<steps>
		<step id="0" description="Create a folder session folder" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="mkdir" location="commandlineTools" >
				<argument id="dirname">
					<value>MASKFACE</value>
				</argument>
			</resource>
		</step>
		<step id="1" description="Create folder for each series in MASKFACE subfolder" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE')^">
			<resource name="mkdir" location="commandlineTools" >
				<argument id="dirname">
					<value>^PIPELINE_LOOPON(series)^</value>
				</argument>
			</resource>
		</step>
		<step id="2" description="Create the RAW folder" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="mkdir" location="commandlineTools" >
				<argument id="dirname">
					<value>RAW</value>
				</argument>
			</resource>
		</step>
		<step id="3" description="Determine which scans will be skipped">
			<resource name="determine_skipped_scans" location="FaceMasking/resource">
                                <argument id="user">
                                        <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                                </argument>
                                <argument id="password">
                                        <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                                </argument>
                                <argument id="host">
                                        <value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
                                </argument>
                                <argument id="outdir">
                                        <value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series))^</value>
                                </argument>
                                <argument id="project">
                                        <value>^/Pipeline/parameters/parameter[name='project']/values/unique/text()^</value>
                                </argument>
                                <argument id="subject">
                                        <value>^/Pipeline/parameters/parameter[name='subject']/values/unique/text()^</value>
                                </argument>
                                <argument id="session">
                                        <value>^/Pipeline/parameters/parameter[name='sessionId']/values/unique/text()^</value>
                                </argument>
                                <argument id="scan">
                                        <value>^PIPELINE_LOOPON(series)^</value>
                                </argument>
                                <argument id="existing">
                                        <value>^/Pipeline/parameters/parameter[name='existing']/values/unique/text()^</value>
                                </argument>
			</resource>
		</step>
		<step id="4" description="Download the Scan DICOM DATA into the RAW folder" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW/')^" precondition="!EXISTS(^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/README_scan_excluded')^)">
			<resource name="XnatRestClient" location="xnat_tools">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="remote">
					<value>^concat('"/data/archive/projects/',/Pipeline/parameters/parameter[name='project']/values/unique/text(),'/subjects/',/Pipeline/parameters/parameter[name='subject']/values/unique/text(),'/experiments/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'/scans/',PIPELINE_LOOPON(series),'/resources/DICOM/files?format=zip&amp;structure=legacy"')^</value>
				</argument> 
				<argument id="method">
					<value>GET</value>
				</argument>
				<argument id="redirect_output">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW/',PIPELINE_LOOPON(series),'.zip')^</value>
				</argument>	
			</resource>
		</step>
        <step id="5" description="Unzip the downloaded scan" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW/')^" precondition="!EXISTS(^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/README_scan_excluded')^)">
                        <resource name="unzip" location="commandlineTools">
                                <argument id="source">
                                        <value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW/',PIPELINE_LOOPON(series),'.zip')^</value>
                                </argument>
                        </resource>
        </step>		
		<step id="6" description="Run face masking on DICOM files" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series))^"  precondition="!EXISTS(^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/README_scan_excluded')^)">
			<resource name="mask_face" location="FaceMasking/resource">
                <argument id="input">
                    	<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW/',/Pipeline/parameters/parameter[name='sessionId']/values/unique/text(),'/SCANS/',PIPELINE_LOOPON(series),'/DICOM')^</value>
                </argument>
				<argument id="output">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/DICOM')^</value>
				</argument>
				<argument id="bet" >
					<value>^/Pipeline/parameters/parameter[name='usebet']/values/unique/text()^</value>
				</argument>
				<argument id="zip" />
				<argument id="label">
					<value>^PIPELINE_LOOPON(series)^</value>
				</argument>
				<argument id="gc">
					<value>^/Pipeline/parameters/parameter[name='invasiveness']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>
		<step id="7" description="Upload defaced DICOM to XNAT" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series))^" precondition="!EXISTS(^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/README_scan_excluded')^)">
			<resource name="XnatRestClient" location="xnat_tools">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="method">
					<value>PUT</value>
				</argument>
				<argument id="remote">
					<value>^concat('"/data/archive/projects/',/Pipeline/parameters/parameter[name='project']/values/unique/text(),'/subjects/',/Pipeline/parameters/parameter[name='subject']/values/unique/text(),'/experiments/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'/scans/',PIPELINE_LOOPON(series),'/resources/DICOM_DEFACED/files/',PIPELINE_LOOPON(series),'.zip?format=DICOM&amp;content=DICOM_DEFACED&amp;extract=true"')^</value>
				</argument> 
				<argument id="local">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/',PIPELINE_LOOPON(series),'.zip')^</value>
				</argument>
			</resource>
		</step> 
		<step id="8" description="Upload defacing QC snapshots to XNAT" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series))^" precondition="!EXISTS(^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/README_scan_excluded')^)">
			<resource name="XnatRestClient" location="xnat_tools">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="method">
					<value>PUT</value>
				</argument>
				<argument id="remote">
					<value>^concat('"/data/archive/projects/',/Pipeline/parameters/parameter[name='project']/values/unique/text(),'/subjects/',/Pipeline/parameters/parameter[name='subject']/values/unique/text(),'/experiments/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'/scans/',PIPELINE_LOOPON(series),'/resources/DEFACE_QC/files/',PIPELINE_LOOPON(series),'.png?format=PNG&amp;content=MASKFACE_QC_PNG&amp;extract=true"')^</value>
				</argument> 
				<argument id="local">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/maskface/',PIPELINE_LOOPON(series),'_qc.zip')^</value>
				</argument>				
			</resource>
		</step> 
		<step id="9" description="Launch dcm2nii and subsequent pipelines" precondition="^/Pipeline/parameters/parameter[name='runOtherPipelines']/values/unique/text()='Y'^">
			<resource name="XnatRestClient" location="xnat_tools">
				<argument id="host">
						<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="password">
						<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="user">
						<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="method">
						<value>POST</value>
				</argument>
				<argument id="remote">
						<value>^concat('"/data/archive/projects/',/Pipeline/parameters/parameter[name='project']/values/unique/text(),'/pipelines/HCPDefaceDicomToNifti/experiments/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'?match=exact"')^</value>
				</argument>
	         	</resource>
		</step> 
		<step id="END-Notify" description="Notify">
			<resource name="Notifier" location="notifications">
                                <argument id="user">
                                        <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                                </argument>
                                <argument id="password">
                                        <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                                </argument>
				<argument id="to">
					<value>^/Pipeline/parameters/parameter[name='useremail']/values/unique/text()^</value>
				</argument>
				<argument id="cc">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="from">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^concat(/Pipeline/parameters/parameter[name='xnatserver']/values/unique/text(), ' update: face masked DICOM files generated for ',/Pipeline/parameters/parameter[name='sessionId']/values/unique/text() )^</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='mailhost']/values/unique/text()^</value>
				</argument>
				<argument id="body">
					<value>^concat('Dear ',/Pipeline/parameters/parameter[name='userfullname']/values/unique/text(),',&lt;br&gt; &lt;p&gt;', ' Face masked DICOM files have been generated for  ', /Pipeline/parameters/parameter[name='sessionId']/values/unique/text(),' . Details of the  session are available  &lt;a href="',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'/app/action/DisplayItemAction/search_element/xnat:mrSessionData/search_field/xnat:mrSessionData.ID/search_value/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'"&gt;', ' here. &lt;/a&gt; &lt;/p&gt;&lt;br&gt;', ' &lt;/p&gt;&lt;br&gt;', 'XNAT Team.')^
					</value>
				</argument>
			</resource>
		</step>
	</steps>
</Pipeline>


