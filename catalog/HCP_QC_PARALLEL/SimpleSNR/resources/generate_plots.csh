#!/bin/csh 
set echo 
set session = $argv[1]
set wrkdir = $argv[2]
set outdir = $argv[3]
set scanid = $argv[4]

set title = "$session SCAN $scanid"



foreach fname ( $wrkdir/*_simpleSNR_4.txt )
 	set filename = `basename $fname .txt`
        tail -n +2 $fname | sed '$d'| cut -d" " -f2 > $wrkdir/${filename}_stripped.dat
	set tSNR = `tail -1 $fname | awk '{print $2}'`
	echo $tSNR
	gnuplot -persist <<PLOT
	set terminal gif small size 640,480 \\
			       xffffff x000000 x404040 \\
			       xff0000 xffa500 x66cdaa xcdb5cd \\
			       xadd8e6 x0000ff xdda0dd x9500d3    # defaults
	set key inside left top vertical Right noreverse enhanced box linetype -1 
	set xlabel "Volume"
	set ylabel "SNR"
	set output '$outdir/${filename}_SNR.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:2 title "${title}: Volume vs SNR (tSNR=$tSNR)"  with lines

	set ylabel "sigMean"
	set output '$outdir/${filename}_sigMean.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:3 title "${title}: Volume vs sigMean"  with lines

	set ylabel "sigMedian"
	set output '$outdir/${filename}_sigMedian.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:4 title "${title}: Volume vs sigMedian"  with lines

	set ylabel "sigStd"
	set output '$outdir/${filename}_sigStd.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:5 title "${title}: Volume vs sigStd"  with lines

	set ylabel "bgMean"
	set output '$outdir/${filename}_bgMean.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:6 title "${title}: Volume vs bgMean"  with lines

	set ylabel "bgMedian"
	set output '$outdir/${filename}_bgMedian.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:7 title "${title}: Volume vs bgMedian"  with lines

	set ylabel "bgStd"
	set output '$outdir/${filename}_bgStd.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:8 title "${title}: Volume vs bgStd"  with lines


	set terminal gif small size 300,300 \\
			       xffffff x000000 x404040 \\
			       xff0000 xffa500 x66cdaa xcdb5cd \\
			       xadd8e6 x0000ff xdda0dd x9500d3    # defaults
	set output '$outdir/${filename}_SNR_thumb.gif'
	set xlabel "Volume"
	set ylabel "SNR"
	plot '$wrkdir/${filename}_stripped.dat' using 1:2 title "${title}: Volume vs SNR (tSNR=$tSNR)"  with lines

	set ylabel "sigMean"
	set output '$outdir/${filename}_sigMean_thumb.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:3 title "${title}: Volume vs sigMean"  with lines

	set ylabel "sigMedian"
	set output '$outdir/${filename}_sigMedian_thumb.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:4 title "${title}: Volume vs sigMedian"  with lines

	set ylabel "sigStd"
	set output '$outdir/${filename}_sigStd_thumb.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:5 title "${title}: Volume vs sigStd"  with lines

	set ylabel "bgMean"
	set output '$outdir/${filename}_bgMean_thumb.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:6 title "${title}: Volume vs bgMean"  with lines

	set ylabel "bgMedian"
	set output '$outdir/${filename}_bgMedian_thumb.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:7 title "${title}: Volume vs bgMedian"  with lines

	set ylabel "bgStd"
	set output '$outdir/${filename}_bgStd_thumb.gif'
	plot '$wrkdir/${filename}_stripped.dat' using 1:8 title "${title}: Volume vs bgStd"  with lines

	quit
	PLOT

end


exit 0
