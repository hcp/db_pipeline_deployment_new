#!/bin/bash

#last update 7/6/06

debug=0
tal=0
skull=0
wm=0
aseg=0
customhtml=0
startslice=35
endslice=215
USAGE="
USAGE: takesnapshotpreset.sh [options...] subject(s)
Options:
    
    -t: talairach preset
    -s: skull strip preset
    -w: white matter preset
    -a: aseg preset
    -c: custom html index file
"

while 
  getopts tswadc opt
do  
   case "$opt"
   in
   d) debug=1;;
   t) tal=1;;
   s) skull=1;;
   w) wm=1;;
   a) aseg=1;;
   c) customhtml=1;;
   \?)  echo "$USAGE"
         exit 1;;
   esac
done

if [ ! $OPTIND -eq 0 ]; then
    shift $(($OPTIND - 1 ))
fi

if [ -z "$1" ]; then
    echo "$USAGE"
    exit 1
fi

subjlist="$1"

#create script
echo -e "set dir [string range [GetSubjectDir 0] 0 [expr [string last \"mri\" [GetSubjectDir 0]] - 2] ]" > snap_preset_tmp.tcl

filename=""

if [ $skull == 1 ]; then
 #   cat ~dkoh/scripts/preset_skull.tcl >> snap_preset_tmp.tcl
fi

if [ $tal == 1 ]; then
  #  cat ~dkoh/scripts/preset_tal.tcl >> snap_preset_tmp.tcl
fi

if [ $wm == 1 ]; then
   # cat ~dkoh/scripts/preset_wm.tcl >> snap_preset_tmp.tcl
fi

if [ $aseg == 1 ]; then
   # cat ~dkoh/scripts/preset_aseg.tcl >> snap_preset_tmp.tcl
fi

#echo "exit" >> snap_preset_tmp.tcl

for s in $subjlist
do
    tkmedit $s brainmask.mgz -aux T1.mgz -tcl snap_preset_tmp.tcl
    
    echo "Converting files to .gif ..."
    filelist=$( ls -1 $SUBJECTS_DIR/$s/rgb/snaps | grep ".*\.rgb" | cut -d "." -f 1 )
    for f in $filelist
    do
        convert $SUBJECTS_DIR/$s/rgb/snaps/${f}.rgb $SUBJECTS_DIR/$s/rgb/snaps/${f}.gif
	rm $SUBJECTS_DIR/$s/rgb/snaps/${f}.rgb
    done
    echo "Done converting files."

if [ $skull == 1 ]; then
    filename="$SUBJECTS_DIR/$s/rgb/snaps/$s-skullstrip-QA.html"
    QAname="skull strip"
    type="skull"
    echo "<html>
    <head>
	<title>$s $QAname QA</title>
    </head>
    <body>" > $filename

    #the following while loop puts the snapshots in numerical order
    #ls would by default order them in an unconventional fashion
    tmplist=`ls -1 $SUBJECTS_DIR/$s/rgb/snaps/ | grep "snapshot-$type-C-[0-9][0-9]*.gif"`
    snaplist=""
    num=$startslice

    while [ $num -le $endslice ]; do
	snapname=`echo $tmplist | grep -o "snapshot-$type-C-$num.gif" | sort -d`
	snaplist="$snaplist $snapname"
	num=$(($num + 1))
    done

    #insert .gif files into html file with captions
    for snap in $snaplist; do
	snapnum=`echo $snap | grep -o "[0-9][0-9]*"`
	echo -e "   <img src=\"$snap\" />
   <p>Slice $snapnum</p>
   <br />" >> $filename
    done

    echo "   </body>
</html>" >> $filename
fi

if [ $tal == 1 ]; then
    filename="$SUBJECTS_DIR/$s/rgb/snaps/$s-talairach-QA.html"
	echo "<html>
    <head>
       <title>$s Talariach QA</title>
    </head>
    <body>

    <img src=\"$SUBJECTS_DIR/$s/rgb/snaps/snapshot-talairach-C-128.gif\" /><br />
    <p>Coronal View</p>
    <img src=\"$SUBJECTS_DIR/$s/rgb/snaps/snapshot-talairach-H-128.gif\" /><br />
    <p>Horizontal View</p>
    <img src=\"$SUBJECTS_DIR/$s/rgb/snaps/snapshot-talairach-S-124.gif\" /><br />
    <p>Parasagittal View - Slice 124</p>
    <img src=\"$SUBJECTS_DIR/$s/rgb/snaps/snapshot-talairach-S-132.gif\" /><br />
    <p>Parasagittal View - Slice 132</p>

    <br />

    </body>
</html>" > $filename
fi

if [ $wm == 1 ]; then
    filename="$SUBJECTS_DIR/$s/rgb/snaps/$s-whitematter-QA.html"
    QAname="white matter"
    type="wm"
    echo "<html>
    <head>
	<title>$s $QAname QA</title>
    </head>
    <body>
    <table"' WIDTH="100%">' > $filename

    #the following while loop puts the snapshots in numerical order
    #ls would by default order them in an unconventional fashion
    tmplist=`ls -1 $SUBJECTS_DIR/$s/rgb/snaps | grep "snapshot-$type-C-[0-9][0-9]*.gif"`
    snaplist=""
    num=$startslice

    while [ $num -le $endslice ]; do
	snapname=`echo $tmplist | grep -o "snapshot-$type-C-$num.gif" | sort -d`
	snaplist="$snaplist $snapname"
	num=$(($num + 1))
    done

    #insert .gif files into html file with captions
    for snap in $snaplist; do
	snapnum=`echo $snap | grep -o "[0-9][0-9]*"`
        echo '<tr HEIGHT=770 ALIGN="CENTER" VALIGN="MIDDLE">' >> $filename
	echo '<td>' >> $filename
	echo -e "   <img src=\"$snap\" />
   <p>Slice $snapnum</p>
   <br />" >> $filename
	echo '</tr>
              </td>' >> $filename
    done

    echo "    </table>
   </body>
</html>" >> $filename
fi

if [ $aseg == 1 ]; then
    filename="$SUBJECTS_DIR/$s/rgb/snaps/$s-aseg-QA.html"
    echo "<html>
    <head>
       <title>$s Aseg QA</title>
    </head>
    <body>

    <img src=\"$SUBJECTS_DIR/$s/rgb/snaps/snapshot-aseg-C-128.gif\" /><br />
    <p>Coronal View</p>
    <img src=\"$SUBJECTS_DIR/$s/rgb/snaps/snapshot-aseg-H-128.gif\" /><br />
    <p>Horizontal View</p>
    <img src=\"$SUBJECTS_DIR/$s/rgb/snaps/snapshot-aseg-S-124.gif\" /><br />
    <p>Parasagittal View - Slice 124</p>
    <img src=\"$SUBJECTS_DIR/$s/rgb/snaps/snapshot-aseg-S-132.gif\" /><br />
    <p>Parasagittal View - Slice 132</p>

    <br />

    </body>
</html>" > $filename
fi

#create index file with all created html files
if [ $customhtml -eq 0 ]; then
    htmlfiles=`ls -1 $SUBJECTS_DIR/$s/rgb/snaps/ | grep ".html" | grep "$s"`

    filename="$SUBJECTS_DIR/$s/rgb/snaps/$s-index.html"
        echo "<html>
    <head>
       <title>$s index</title>
    </head>
    <body>" > $filename

    for file in $htmlfiles
    do

        if [ -z "`echo $file | grep 'QA'`" ]; then

	    if [ "$file" == "${filename##/*/}" ]; then
		continue
	    fi

	    echo -en "   <a href=\"$file\">" >> $filename
	    printtype=`echo $file | awk -F "-" '{print $2}'`
	    orienttag=`echo $file | awk -F "-" '{print $3}'`

	    if [ ! -z "$orienttag" ]; then
		case "$orienttag"
		in
		    C) printorient="coronal";;
		    H) printorient="horizontal";;
		    S) printorient="sagittal";;
		esac
	    else
		printorient=""
	    fi

	    echo -e "$s $printorient $printtype </a> <br />" >> $filename
	else
	    echo -en "   <a href=\"$file\">" >> $filename
	    qatype=`echo $file | awk -F "-" '{print $2}'`
	    echo -e "$s $qatype QA </a> <br />" >> $filename
	fi
    done

    echo "   </body>
    </html>" >> $filename
fi
#end subj loop
done

    
