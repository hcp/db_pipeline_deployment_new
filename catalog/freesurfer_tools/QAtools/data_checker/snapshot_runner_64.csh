#!/bin/tcsh -f

# 
# Runner for snap_montage_fs5.csh, assume this script, snap_montage_fs5.csh and xvfb_wrapper.sh resides in the same directory
# 

## Just return version number and exit
if ( $# < 2 ) then
   echo "1.0"
   exit
endif

# get script directory (for location of tcl script in same directory)
set scriptdir=`dirname $0`
set scriptdir=`readlink -f $scriptdir`

set subject=$1
set subjdir=$2

if ( -e $subjdir ) then

   $scriptdir/xvfb_wrapper.sh $scriptdir/snap_montage_fs5.csh $subject $subjdir

endif

