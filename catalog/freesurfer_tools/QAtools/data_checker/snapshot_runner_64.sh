#!/bin/bash

# get script directory (for location of tcl script in same directory)

if [ $#  -lt 2 ] ; then
   ## Just return version number and exit
   exit 1;
fi

scriptdir=`dirname $0`
scriptdir=`readlink -f $scriptdir`

subject=$1
subjdir=$2

# 
# Runner for snap_montage_fs5.csh, assume this script, snap_montage_fs5.csh and xvfb_wrapper.sh resides in the same directory
# 

#FREESURFER_HOME=/nrgpackages/packages/freesurfer5; export FREESURFER_HOME
FREESURFER_HOME=/nrgpackages/tools.release/freesurfer-Linux-centos4_x86_64-stable-pub-v5.1.0; export FREESURFER_HOME
PATH=$PATH:$FREESURFER_HOME/bin; export PATH

## 
## GRID ENGINE SETUP
## 

SGE_ROOT=/nrgpackages/sge_root; export SGE_ROOT

ARCH=`$SGE_ROOT/util/arch`
echo arch=$ARCH
DEFAULTMANPATH=`$SGE_ROOT/util/arch -m`
MANTYPE=`$SGE_ROOT/util/arch -mt`

SGE_CELL=nrg; export SGE_CELL
SGE_CLUSTER_NAME=nrg; export SGE_CLUSTER_NAME
SGE_QMASTER_PORT=6444; export SGE_QMASTER_PORT
SGE_EXECD_PORT=6445; export SGE_EXECD_PORT

if [ "$MANPATH" = "" ]; then
   MANPATH=$DEFAULTMANPATH
fi
MANPATH=$SGE_ROOT/$MANTYPE:$MANPATH; export MANPATH

PATH=$SGE_ROOT/bin/$ARCH:$PATH; export PATH

echo SGE_ROOT=$SGE_ROOT
echo ARCH=$ARCH
echo DEFAULTMANPATH=$DEFAULTMANPATH
echo MANTYPE=$MANTYPE
echo SGE_CELL=$SGE_CELL
echo SGE_CULSTER_NAME=$SGE_CULSTER_NAME


# library path setting required only for architectures where RUNPATH is not supported
case $ARCH in
sol*|lx*|hp11-64)
   ;;
*)
   shlib_path_name=`$SGE_ROOT/util/arch -lib`
   old_value=`eval echo '$'$shlib_path_name`
   if [ x$old_value = x ]; then
      eval $shlib_path_name=$SGE_ROOT/lib/$ARCH
   else
      eval $shlib_path_name=$SGE_ROOT/lib/$ARCH:$old_value
   fi
   export $shlib_path_name
   unset shlib_path_name old_value
   ;;
esac
unset ARCH DEFAULTMANPATH MANTYPE

## 
## END GRID ENGINE SETUP
## 

if [ -d $subjdir ] ; then

   #### non-grid submit for testing 
   ##$scriptdir/xvfb_wrapper.sh $scriptdir/snap_montage_fs5.csh $subject $subjdir

   ## SUBMIT TO GRID
   SOUT=`qsub -V $scriptdir/xvfb_wrapper.sh $scriptdir/snap_montage_fs5.csh $subject $subjdir`

   JOBID=`echo $SOUT | awk '{print $3}'`

   echo "SGE JOBID=$JOBID"
  
   ## MONITOR JOB

   maxiter=60
   iter=1
   while [ $iter -lt $maxiter ] ; do
      sleep 60
      STATUS=`qstat | awk '{print $1 " " $5}' | grep $JOBID | grep -v "E"`
      if [ -z "$STATUS" ] ; then
         ##echo "DONE";
         exit 0;
      ##else 
         ##echo "PROCESSING";
      fi
      let iter+=1
   done
   
   exit 1;

fi

exit 1;

