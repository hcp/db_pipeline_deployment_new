#!/bin/tcsh -f

#Author: Mohana Ramaratnam
#This script will generate the gif snapshots of 
#brainmask.mgz, wm.mgz, T1.mgz and aseg.mrg 
#for a subject. 
#
#Depends on a TCL script to generate the appropriate slices

set me=$0:t

if ($# == 0) then
 goto USAGE
endif

goto Start

##############################################
USAGE: 
	echo "$me <subjectid> [<Path to subjects directory>]"
	echo "This script creates the snapshot images of the aseg, brainmask, T1 and wm volumes."
	echo "Defaults to environmental variable SUBJECTS_DIR "
	echo "Prerequisites: Needs FREESURFER_HOME environment variable to be set"
	exit
##############################################


Start:

if (! ${?RECON_CHECKER_SCRIPTS}) then
  if (! ${?FREESURFER_HOME}) then
   echo "$me need RECON_CHECKER_SCRIPTS or FREESURFER_HOME env var set"
   exit  
  else 
     setenv RECON_CHECKER_SCRIPTS $FREESURFER_HOME/QAtools/data_checker
  endif
endif

set subject = $1
set subjects_dir = $SUBJECTS_DIR

if ($# == 2) then 
  set subjects_dir = $2
endif

setenv SUBJECTS_DIR $subjects_dir


cd $subjects_dir


set spath = $subjects_dir/$subject


if (! -e $spath/rgb/snaps) then
  mkdir -p $spath/rgb/snaps
endif

if ( -e "$spath/mri/brainmask.mgz" ) then
 setenv PREFIX brnmsk
 setenv LOADOTHERS 0
 setenv ASEG 0
 tkmedit $subject brainmask.mgz -tcl $RECON_CHECKER_SCRIPTS/snap_montage.tcl
endif


if ( -e "$spath/mri/T1.mgz" ) then
 setenv PREFIX t1
 setenv LOADOTHERS 1
 setenv ASEG 0
 tkmedit $subject T1.mgz -tcl $RECON_CHECKER_SCRIPTS/snap_montage.tcl
endif

if ( -e "$spath/mri/t1.mgz" ) then
 setenv PREFIX t1
 setenv LOADOTHERS 1
 setenv ASEG 0
 tkmedit $subject t1.mgz -tcl $RECON_CHECKER_SCRIPTS/snap_montage.tcl
endif


if ( -e "$spath/mri/wm.mgz" ) then
  setenv PREFIX wm
  setenv LOADOTHERS 0
  setenv ASEG 0
 tkmedit $subject wm.mgz -tcl $RECON_CHECKER_SCRIPTS/snap_montage.tcl
endif

if ( -e "$spath/mri/aseg.mgz" ) then
  setenv PREFIX aseg
  setenv LOADOTHERS 0
  setenv ASEG 1
  tkmedit $subject brainmask.mgz  -tcl $RECON_CHECKER_SCRIPTS/snap_montage.tcl
endif

#Create gif and delete the rgb files

pushd $spath/rgb/snaps
  if (`ls | egrep -c "\.rgb"`) then
    foreach f (*.rgb)
       convert -scale 300x300 $f ${f:r}.gif
       rm $f
    end 
  endif
popd

echo "$me all done"
