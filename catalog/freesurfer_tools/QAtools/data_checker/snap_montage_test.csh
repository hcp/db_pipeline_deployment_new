set subject    [GetSubjectName 0]

global env
set subjectsdir $env(SUBJECTS_DIR)
set subjectdir "${subjectsdir}/${subject}"

set    imagedir "${subjectsdir}/rgb/snaps"

puts "Subject    : $subject"
puts "Subject Dir: $subjectdir"

SetZoomLevel 1
RedrawScreen
set outpth "${subjectdir}/rgb/snaps"

puts "Load main surfaces"
LoadMainSurface      0 lh.white
LoadPialSurface      0 lh.pial
LoadOriginalSurface  0 lh.orig

puts "Load aux surfaces"
LoadMainSurface      1 rh.white
LoadPialSurface      1 rh.pial
LoadOriginalSurface  1 rh.orig

puts "SetDisplayFlags"
# SetDisplayFlag  2 1
SetDisplayFlag    4 1
SetDisplayFlag    5 1
SetDisplayFlag    6 1
SetDisplayFlag   22 1

puts "At the method"

exit

