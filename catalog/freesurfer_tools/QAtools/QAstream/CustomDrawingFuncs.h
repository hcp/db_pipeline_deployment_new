#ifndef CUSTOM_DRAWING_FUNCS_H
#define CUSTOM_DRAWING_FUNCS_H

#include <gtk/gtk.h>
#include <string>
#include <vector>

using std::string;
using std::vector;

extern void pickOptions( GtkWidget *window, int &option, const vector<string*> customOpts );
extern GtkWidget * drawOptions( GtkWidget *window, int &option, const vector<string*> customOpts );
extern GtkWidget * drawQA( const int option, GtkWidget *window, string &choice_data, string filename, string user, const vector<string> * const subjectList, const vector<string*> * customOptsPtr, const vector<string::size_type> customOptLen, const vector<int> fieldWidths );

#endif
