#ifndef GTK_EVENT_HANDLERS
#define GTK_EVENT_HANDLERS

#include <gtk/gtk.h>

//Definitions for the event handlers, or by GTK terminology, the 
//callback functions associated with various widgets.  Most are defined
//inside of QAnotepad.cpp, though the submit_data functions are defined
//in their respective files because they're closely integrated with the
//interface.

//Defined in QAnotepad.cpp
extern gboolean delete_event( GtkWidget *widget, GdkEvent *event, gpointer data );
extern gboolean destroy( GtkWidget *widget, gpointer data );
extern void delete_window( GtkWidget *widget, gpointer data );
extern void submit_option( GtkWidget *button, gpointer opt );
extern void set_data( GtkWidget *widget, gpointer data );
extern void set_freetext_data( GtkWidget *widget, gpointer data );

//Defined in DrawingFuncs.cpp
extern void submit_data( GtkWidget *widget, gpointer data );

//Defined in CustomDrawingFuncs.cpp
extern void submit_data_custom( GtkWidget *button, gpointer data );

#endif
