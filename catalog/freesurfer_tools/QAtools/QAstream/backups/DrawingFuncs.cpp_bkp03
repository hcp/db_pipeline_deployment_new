#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <gtk/gtk.h>

#include "QA_Option.h"
#include "Data.h"
#include "GTKEventHandlers.h"
#include "DrawingFuncs.h"
#include "Config.h"

using std::cout;
using std::cerr;
using std::endl;
using std::setw;
using std::left;
using std::string;
using std::ifstream;
using std::ofstream;
using std::istringstream;
using std::ostringstream;
using std::vector;
using std::iterator;

//----------------------------
//--- Non-Custom Functions ---
//----------------------------

void pickOptions( GtkWidget *window, int &option )
{
    GtkWidget *options = drawOptions( window, option );
    gtk_container_add( GTK_CONTAINER( window ), options );
}

void submit_data( GtkWidget *button, gpointer data )
{
    Data *choice_data = static_cast<Data*>(data);
    string filename = choice_data->filename();
    string user = choice_data->username();
    vector<string> raw_data;
    ifstream infile;
   
    infile.open( filename.c_str() );
    if( !infile ) {
        cout << "Unable to open input file: " << filename << endl;
        cout << "Creating " << filename << "..." << endl;
	ofstream newfile( filename.c_str() );
	if( !newfile )
	    cerr << "Error: Unable to create file: " << filename << endl;
	else {
	    newfile << left 
		    << setw(FIELDWIDTH) << "Subject" 
		    << setw(FIELDWIDTH) << "User"
		    << setw(FIELDWIDTH) << "Talairach" 
		    << setw(FIELDWIDTH) << "Skull_Strip" 
		    << setw(FIELDWIDTH) << "White_Matter"
		    << "Aseg\n";
	    string subject( static_cast<const char*>( gtk_entry_get_text( GTK_ENTRY( choice_data->subjectField() ) ) ) );
	    //big comments, so I can find where the data is written
	    //##############################
	    //### WRITE DATA TO NEW FILE ###
	    //##############################
	    newfile << left 
		    << setw(FIELDWIDTH) << subject
		    << setw(FIELDWIDTH) << user;
	    for( int pos = 1; pos < NUM_FIELDS + 1; ++pos ) {
	        if( pos == choice_data->choice() )
		    newfile << left << setw(FIELDWIDTH) << choice_data->data();
		else
		    newfile << left << setw(FIELDWIDTH) << "-";
	    }
	    newfile << endl;
	    newfile.close();
	}
    }
    else {
        int nextIndex = 0;
	string line;
	while( !infile.eof() ) {
	    getline( infile, line );
	    if( line == "" )
	        continue;
	    line += "\n";
	    raw_data.push_back( line );
	    ++nextIndex;
	}
	infile.close();
	//vector<string>::iterator dItr;
	//for( dItr = raw_data.begin(); dItr != raw_data.end(); ++dItr )
	//    cout << *dItr;
	//cout << endl;

	string subject( static_cast<const char*>( gtk_entry_get_text( GTK_ENTRY( choice_data->subjectField() ) ) ) );
	vector<string>::iterator itr;
	istringstream strm;
	ostringstream newline;
	string word;
	bool wordFound = false;
        for( itr = raw_data.begin(); itr != raw_data.end(); ++itr ) {
	    strm.str( itr->c_str() );
	    strm >> word;
	    // if the subject already exists, update the line
	    if( word == subject ) {
		//####################################
		//### UPDATE DATA IN EXISTING FILE ###
		//####################################
		//write subject
	        newline << left << setw(FIELDWIDTH) << word;
		//write user
		strm >> word;
		newline << left << setw(FIELDWIDTH) << user;
		for( int i = 1; i < choice_data->choice(); ++i ) {
		    strm >> word;
		    newline << left << setw(FIELDWIDTH) << word;
		}
		strm >> word;
		newline << left << setw(FIELDWIDTH) << choice_data->data();
		for( int i = choice_data->choice() + 1; i < NUM_FIELDS + 1 ; ++i ) {
		    strm >> word;
		    newline << left << setw(FIELDWIDTH) << word;
		}
		newline << endl;
		*itr = newline.str();
		wordFound = true;
	        break;
	    }
	}
	ofstream outfile;
	if( wordFound ) {
	    outfile.open( filename.c_str() );
	    if( !outfile )
	        cerr << "Error: Unable to write to file: " << filename << endl;
	    else {
	        for( itr = raw_data.begin(); itr != raw_data.end(); ++itr )
		    outfile << *itr;
		outfile.close();
	    }
	}
	else {
	    //open for append
	    outfile.open( filename.c_str(), ofstream::app );
	    if( !outfile )
	        cerr << "Error: Unable to write to file: " << filename << endl;
	    else {
	        string subject( static_cast<const char*>( gtk_entry_get_text( GTK_ENTRY( choice_data->subjectField() ) ) ) );
		//#####################################
		//### ADD NEW DATA TO EXISTING FILE ###
		//#####################################
		outfile << left 
			<< setw(FIELDWIDTH) << subject
			<< setw(FIELDWIDTH) << user;
		for( int pos = 1; pos < NUM_FIELDS + 1; ++pos ) {
		    if( pos == choice_data->choice() )
			outfile << left << setw(FIELDWIDTH) << choice_data->data();
		    else
			outfile << left << setw(FIELDWIDTH) << "-";
		}
		outfile << endl;
		outfile.close();
	    }
	}
    }

    delete_window( GTK_WIDGET( choice_data->window() ), NULL );
}

// drawing functions

GtkWidget * drawQA( const int option, GtkWidget *window, string &choice_data, string filename, string user, const vector<string> * const subjectList )
{
    GtkWidget *mainVbox, *mainHbox, *pairVbox, *frame;
    
    switch ( option ) {
    case 1:
        frame = gtk_frame_new( "Talairach Options" );
	choice_data = "Good";
	break;
    case 2:
        frame = gtk_frame_new( "Skull Strip Options" );
	choice_data = "Good";
	break;
    case 3:
        frame = gtk_frame_new( "White Matter Options" );
	choice_data = "Good";
	break;
    case 4:
        frame = gtk_frame_new( "Auto Segmentation Options" );
	choice_data = "Good";
	break;
    }

    mainVbox = gtk_vbox_new( TRUE, VERT_SPACING );
    mainHbox = gtk_hbox_new( TRUE, HORIZ_SPACING );
    pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );

    GtkWidget *label, *rbutton;
    GSList *rgroup;
    
    switch ( option ) {
    case 1: 
        label = gtk_label_new( "Good" );
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( NULL );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Good", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
        label = gtk_label_new( "Bad" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Bad", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
	label = gtk_label_new( "Ugly" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Ugly", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	break;
    case 2:
        label = gtk_label_new( "Good" );
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( NULL );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Good", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
        label = gtk_label_new( "Too Much" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Too_Much", &choice_data ));
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
	label = gtk_label_new( "Too Little" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Too_Little", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	break;
    case 3:
        label = gtk_label_new( "Good" );
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( NULL );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Good", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
        label = gtk_label_new( "Corrected" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Corrected", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
	label = gtk_label_new( "Needs Correction" );
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Needs_Correction", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
        label = gtk_label_new( "Stuck" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Stuck", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
	label = gtk_label_new( "Exclude" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Exclude", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	break;
    case 4:
        label = gtk_label_new( "Good" );
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( NULL );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Good", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
	label = gtk_label_new( "Good for Vol" );
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Good_for_Vol", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
        label = gtk_label_new( "Good for Func" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Good_for_Func", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
	label = gtk_label_new( "Needs Editing" );
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "Needs_Editing", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
        label = gtk_label_new( "lhip" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "lhip", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton ); 	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
	label = gtk_label_new( "N/A" );	
	gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	gtk_widget_show( label );
	rbutton = gtk_radio_button_new( rgroup );
	g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( "N/A", &choice_data ));
	gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	gtk_widget_show( rbutton );	
	gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	gtk_widget_show( pairVbox );
	break;
    }
    gtk_box_pack_start( GTK_BOX( mainVbox ), mainHbox, TRUE, FALSE, 0 );
    gtk_widget_show( mainHbox );

    GtkWidget* subjectField;

    if( subjectList->empty() ) {
	subjectField = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY( subjectField ), "Enter_Subject" );
	gtk_editable_select_region ( GTK_EDITABLE( subjectField ), 0, GTK_ENTRY( subjectField )->text_length );
	gtk_box_pack_start( GTK_BOX( mainVbox ), subjectField, TRUE, FALSE, 0 );
	gtk_widget_show( subjectField );
    }
    else {
	GtkWidget* combo = gtk_combo_new();    
	GList *comboList = NULL;
	
	vector<string>::const_iterator itr;
	for( itr = subjectList->begin(); itr != subjectList->end(); ++itr )
	    comboList = g_list_append( comboList, const_cast<char*>( itr->c_str() ) );
	gtk_combo_set_popdown_strings (GTK_COMBO (combo), comboList);
	
	gtk_box_pack_start( GTK_BOX( mainVbox ), combo, TRUE, FALSE, 0 );
	gtk_widget_show( combo );
	subjectField = GTK_COMBO( combo )->entry;
    }

    GtkWidget* submit = gtk_button_new_with_label( "Submit" );
    g_signal_connect( G_OBJECT( submit ), "clicked", G_CALLBACK( submit_data ), new Data( window, subjectField, option, choice_data, filename, user ) );
    gtk_box_pack_start( GTK_BOX( mainVbox ), submit, TRUE, FALSE, 0 );
    gtk_widget_show( submit );

    gtk_container_add( GTK_CONTAINER( frame ), mainVbox );
    gtk_widget_show( mainVbox );
    gtk_widget_show( frame );
    return frame;
}

GtkWidget * drawOptions( GtkWidget* window, int &option )
{
    GtkWidget *vbox;
    vbox = gtk_vbox_new( TRUE, VERT_SPACING );
    
    GtkWidget *separator = gtk_hseparator_new();
    gtk_box_pack_start( GTK_BOX( vbox ), separator, TRUE, FALSE, 0 );
    gtk_widget_show( separator );

    GtkWidget *button;
    button = gtk_button_new_with_label( "Talaraich" );
    g_signal_connect( G_OBJECT( button ), "clicked", G_CALLBACK( submit_option ), new QA_Option( 1, &option, window ));
    gtk_widget_show( button );

    gtk_box_pack_start( GTK_BOX( vbox ), button, TRUE, FALSE, 0 );
    
    button = gtk_button_new_with_label( "Skull Strip" );
    g_signal_connect( G_OBJECT( button ), "clicked", G_CALLBACK( submit_option ), new QA_Option( 2, &option, window ) );
    gtk_widget_show( button );

    gtk_box_pack_start( GTK_BOX( vbox ), button, TRUE, FALSE, 0 );
    
    button = gtk_button_new_with_label( "White Matter" );
    g_signal_connect( G_OBJECT( button ), "clicked", G_CALLBACK( submit_option ), new QA_Option( 3, &option, window ) );
    gtk_widget_show( button );

    gtk_box_pack_start( GTK_BOX( vbox ), button, TRUE, FALSE, 0 );
								    
    button = gtk_button_new_with_label( "Automatic Segmentation" );
    g_signal_connect( G_OBJECT( button ), "clicked", G_CALLBACK( submit_option ), new QA_Option( 4, &option, window ) );
    gtk_widget_show( button );

    gtk_box_pack_start( GTK_BOX( vbox ), button, TRUE, FALSE, 0 );

    separator = gtk_hseparator_new();
    gtk_box_pack_start( GTK_BOX( vbox ), separator, TRUE, FALSE, 0 );
    gtk_widget_show( separator );
    gtk_widget_show( vbox );

    return vbox;
}
