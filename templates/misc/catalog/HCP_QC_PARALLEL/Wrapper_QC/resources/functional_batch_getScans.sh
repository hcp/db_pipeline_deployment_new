#!/bin/bash -e

#Given a params file and an index of the scan as input argument,
#this script would get the NIFTI for a given scan
#The scan is picked up from the array using the $SGE_TASK_ID for the given task

my_sge_task_id=$1
paramsFile=$2
catalogName=$3

source $paramsFile


XNATRESTCLIENT=@PIPELINE_DIR_PATH@/xnat-tools/XNATRestClient

my_scan=${functional_usable_scanids[$my_sge_task_id]}

restPath=${aliasHost}data/archive/experiments/${xnat_id}/scans/${my_scan}/resources/$catalogName/files?format=zip

outFile=${workdir}/RAW${catalogName}/${my_scan}/${my_scan}.zip

destinationPath="${workdir}/RAW${catalogName}/${my_scan}"

curl -k -u ${user}:${passwd} -G "$restPath" > $outFile


#$XNATRESTCLIENT -host $aliasHost -u $user -p $passwd -m GET -remote "$restPath" -local $outFile

unzip -oj $outFile -d $destinationPath

\rm -f $outFile

exit 0;
