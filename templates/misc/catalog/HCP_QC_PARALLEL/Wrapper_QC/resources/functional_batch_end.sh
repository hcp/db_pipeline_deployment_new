#!/bin/bash -e

#This script will submit to the SGE jobs to process the Funcational scans

paramsFile=$1


source $paramsFile

BINDIR=@PIPELINE_DIR_PATH@/bin
PIPELINE=@PIPELINE_DIR_PATH@/catalog/HCP_QC_PARALLEL/Wrapper_QC/FunctionalLevel2QC_End_v1.0.xml

$BINDIR/XnatPipelineLauncher -pipeline $PIPELINE -id $xnat_id -host $aliasHost -u $user -pwd $passwd -dataType xnat:mrSessionData -label $sessionId -supressNotification -project $project -notify $useremail -notify $adminemail -parameter mailhost=$mailhost -parameter xnat_id=$xnat_id -parameter project=$project -parameter sessionId=$sessionId -parameter userfullname=$userfullname -parameter builddir=$builddir -parameter xnatserver=$xnatserver -parameter adminemail=$adminemail -parameter useremail=$useremail


exit 0;

