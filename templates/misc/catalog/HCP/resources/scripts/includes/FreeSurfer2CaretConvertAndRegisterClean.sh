#!/bin/bash
set -x

FreeSurferTemplateFolder=/export/freesurfer-5.1/subjects/fsaverage #Where your fsaverage is located
FSLTemplateFolder=/export/fsl-4.1.6/data/standard #Where your FSL standard atlases are located
#AtlasFolder=/usr/local/caret/data_files/standard_mesh_atlases #Where your caret standard atlases are located
AtlasFolder=~/pipeline_atlases/standard_mesh_atlases #Where your caret standard atlases are located

Subject="$1" #Subject ID (from FreeSurfer)
CaretFolder="$2" #Folder for Caret Files to go
dataFolder="$3" #Folder for Volume Files to go
FreeSurferFolder="$4" #Folder where your FreeSurfer Processing Results Are for the Individual
T1weightedImage="$5" #Image you fed into FreeSurfer


Species="Human"
DownSampleI="20000" #Approximate mesh downsample resolution
DownSampleNameI="20" #Downsample mesh prefix


if [ ! -e "$dataFolder"/xfms ] ; then
  mkdir -p "$dataFolder"/xfms
fi
if [ ! -e "$CaretFolder" ] ; then
  mkdir "$CaretFolder"
fi
if [ ! -e "$CaretFolder"/xfms ] ; then
  mkdir "$CaretFolder"/xfms
fi
if [ ! -e "$CaretFolder"/Native ] ; then
  mkdir "$CaretFolder"/Native
fi
if [ ! -e "$CaretFolder"/Template ] ; then
  mkdir "$CaretFolder"/Template
fi
if [ ! -e "$CaretFolder"/fsaverage ] ; then
  mkdir "$CaretFolder"/fsaverage
fi

fslmaths "$T1weightedImage" "$T1weightedImage" -odt float
mri_convert -rl "$T1weightedImage" "$FreeSurferFolder"/mri/brain.finalsurfs.mgz "$dataFolder"/brain.finalsurfs.nii.gz
flirt -in "$dataFolder"/brain.finalsurfs.nii.gz -ref "$FSLTemplateFolder"/MNI152_T1_1mm_brain -omat "$dataFolder"/str2standard.mat -out "$dataFolder"/brain.finalsurfs2std.nii.gz
cp "$dataFolder"/str2standard.mat "$dataFolder"/xfms/str2standard.mat
convert_xfm -omat "$dataFolder"/xfms/standard2str.mat -inverse "$dataFolder"/xfms/str2standard.mat
applywarp --interp=spline -i "$T1weightedImage" -r "$FSLTemplateFolder"/MNI152_T1_1mm_brain --premat="$dataFolder"/xfms/str2standard.mat -o "$dataFolder"/T1Struct_With_Skull.nii.gz

cp "$dataFolder"/xfms/* "$CaretFolder"/xfms

gunzip "$dataFolder"/brain.finalsurfs.nii.gz
gunzip "$dataFolder"/brain.finalsurfs2std.nii.gz
aff_conv "$dataFolder"/brain.finalsurfs.nii "$dataFolder"/brain.finalsurfs2std.nii "$dataFolder"/xfms/str2standard.mat "$dataFolder"/xfms/str2standard.wmat fw
gzip "$dataFolder"/brain.finalsurfs.nii
gzip "$dataFolder"/brain.finalsurfs2std.nii

MatrixX=`mri_info "$FreeSurferFolder"/mri/brain.finalsurfs.mgz | grep "c_r" | cut -d "=" -f 5 | sed s/" "/""/g`
MatrixY=`mri_info "$FreeSurferFolder"/mri/brain.finalsurfs.mgz | grep "c_a" | cut -d "=" -f 5 | sed s/" "/""/g`
MatrixZ=`mri_info "$FreeSurferFolder"/mri/brain.finalsurfs.mgz | grep "c_s" | cut -d "=" -f 5 | sed s/" "/""/g`
Matrix1=`echo "1 0 0 ""$MatrixX"`
Matrix2=`echo "0 1 0 ""$MatrixY"`
Matrix3=`echo "0 0 1 ""$MatrixZ"`
Matrix4=`echo "0 0 0 1"`
Matrix=`echo "$Matrix1"" ""$Matrix2"" ""$Matrix3"" ""$Matrix4"`
echo $Matrix

mri_convert -rt nearest -rl "$T1weightedImage" "$FreeSurferFolder"/mri/wmparc.mgz "$dataFolder"/wmparc.nii.gz
applywarp --interp=nn -i "$dataFolder"/wmparc.nii.gz -r "$FSLTemplateFolder"/MNI152_T1_1mm_brain --premat="$dataFolder"/xfms/str2standard.mat -o "$CaretFolder"/wmparc.nii.gz
mri_convert -rl "$T1weightedImage" "$FreeSurferFolder"/mri/brain.finalsurfs.mgz "$dataFolder"/brain.finalsurfs.nii.gz
applywarp --interp=spline -i "$dataFolder"/brain.finalsurfs.nii.gz -r "$FSLTemplateFolder"/MNI152_T1_1mm_brain --premat="$dataFolder"/xfms/str2standard.mat -o "$CaretFolder"/brain.finalsurfs.nii.gz
mri_convert -rt nearest -rl "$T1weightedImage" "$FreeSurferFolder"/mri/ribbon.mgz "$dataFolder"/ribbon.nii.gz
applywarp --interp=nn -i "$dataFolder"/ribbon.nii.gz -r "$FSLTemplateFolder"/MNI152_T1_1mm_brain --premat="$dataFolder"/xfms/str2standard.mat -o "$CaretFolder"/ribbon.nii.gz
mri_convert -rl "$T1weightedImage" "$FreeSurferFolder"/mri/nu.mgz "$dataFolder"/"$Subject".nii.gz
applywarp --interp=spline -i "$dataFolder"/"$Subject".nii.gz -r "$FSLTemplateFolder"/MNI152_T1_1mm_brain --premat="$dataFolder"/xfms/str2standard.mat -o "$CaretFolder"/"$Subject".nii.gz
fslmaths "$CaretFolder"/wmparc.nii.gz -bin "$CaretFolder"/wmparc_mask.nii.gz

for Hemisphere in L R ; do
  if [ $Hemisphere = "L" ] ; then 
    hemisphere="l"
    HEMISPHERE="LEFT"
    hemispherew="left"
  elif [ $Hemisphere = "R" ] ; then 
    hemisphere="r"
    hemispherew="right"
    HEMISPHERE="RIGHT"
  fi
  DIR=`pwd`
  cd $CaretFolder/Template
    caret_command -spec-file-create $Species $Subject $hemispherew OTHER -category Atlas -spec-file-name "$CaretFolder"/Template/fsaverage."$Hemisphere".template.164k_fs.spec
  cd $DIR
  i=1
  for Surface in sphere white pial ; do
    var=`echo "SPHERICAL FIDUCIAL FIDUCIAL" | cut -d " " -f $i`
    caret_command -file-convert -sc -is FSS "$FreeSurferTemplateFolder"/surf/"$hemisphere"h."$Surface" -os CARET "$CaretFolder"/Template/fsaverage."$Hemisphere"."$Surface".164k_fs.coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii $var CLOSED -struct $hemispherew
    caret_command -spec-file-add "$CaretFolder"/Template/fsaverage."$Hemisphere".template.164k_fs.spec "$var"coord_file "$CaretFolder"/Template/fsaverage."$Hemisphere"."$Surface".164k_fs.coord.gii
    i=`echo "$i + 1" | bc`
  done
  cp "$FSLTemplateFolder"/MNI152_T1_1mm.nii.gz "$CaretFolder"/Template/MNI152_T1_1mm.nii.gz
  caret_command -spec-file-add "$CaretFolder"/Template/fsaverage."$Hemisphere".template.164k_fs.spec volume_anatomy_file "$CaretFolder"/Template/MNI152_T1_1mm.nii.gz
  caret_command -spec-file-add "$CaretFolder"/Template/fsaverage."$Hemisphere".template.164k_fs.spec CLOSEDtopo_file "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii
  caret_command -file-convert -fsc2c "$FreeSurferTemplateFolder"/surf/"$hemisphere"h.sulc "$FreeSurferTemplateFolder"/surf/"$hemisphere"h.white "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.shape.gii
  caret_command -spec-file-add "$CaretFolder"/Template/fsaverage."$Hemisphere".template.164k_fs.spec surface_shape_file "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.shape.gii
  caret_command -surface-average "$CaretFolder"/Template/fsaverage."$Hemisphere".midthickness.164k_fs.coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".pial.164k_fs.coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".white.164k_fs.coord.gii
  caret_command -spec-file-add "$CaretFolder"/Template/fsaverage."$Hemisphere".template.164k_fs.spec FIDUCIALcoord_file "$CaretFolder"/Template/fsaverage."$Hemisphere".midthickness.164k_fs.coord.gii
  caret_command -surface-generate-inflated "$CaretFolder"/Template/fsaverage."$Hemisphere".midthickness.164k_fs.coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii -iterations-scale 2.5 -generate-inflated -generate-very-inflated -output-spec "$CaretFolder"/Template/fsaverage."$Hemisphere".template.164k_fs.spec -output-inflated-file-name "$CaretFolder"/Template/fsaverage."$Hemisphere".inflated.164k_fs.coord.gii -output-very-inflated-file-name "$CaretFolder"/Template/fsaverage."$Hemisphere".very_inflated.164k_fs.coord.gii
  caret_command -surface-sphere "$CaretFolder"/Template/fsaverage."$Hemisphere".sphere.164k_fs.coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".sphere.164k_fs.coord.gii

  DIR=`pwd`
  cd $CaretFolder/Native
    caret_command -spec-file-create $Species $Subject $hemispherew OTHER -category Individual -spec-file-name "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec
  cd $DIR
  i=1
  for Surface in white pial ; do
    var=`echo "FIDUCIAL FIDUCIAL" | cut -d " " -f $i`
    caret_command -file-convert -sc -is FSS "$FreeSurferFolder"/surf/"$hemisphere"h."$Surface" -os CARET "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii $var CLOSED -struct $hemispherew
    caret_command -surface-apply-transformation-matrix "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii -matrix $Matrix
    caret_command -surface-apply-transformation-matrix "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii -matrix `sed s/\n/" "/g < "$dataFolder"/xfms/str2standard.wmat`
    caret_command -spec-file-add "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec "$var"coord_file "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii
    i=`echo "$i + 1" | bc`
  done
  i=1
  for Surface in sphere.reg sphere ; do
    var=`echo "SPHERICAL SPHERICAL" | cut -d " " -f $i`
    caret_command -file-convert -sc -is FSS "$FreeSurferFolder"/surf/"$hemisphere"h."$Surface" -os CARET "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii $var CLOSED -struct $hemispherew
    caret_command -surface-apply-transformation-matrix "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii -matrix $Matrix
    caret_command -surface-sphere "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii
    caret_command -spec-file-add "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec "$var"coord_file "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii
    i=`echo "$i + 1" | bc`
  done
  caret_command -spec-file-add "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec volume_anatomy_file "$CaretFolder"/"$Subject".nii.gz
  caret_command -spec-file-add "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec CLOSEDtopo_file "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii
  caret_command -file-convert -fsc2c "$FreeSurferFolder"/surf/"$hemisphere"h.sulc "$FreeSurferFolder"/surf/"$hemisphere"h.white "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.shape.gii
  caret_command -spec-file-add "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec surface_shape_file "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.shape.gii
  caret_command -file-convert -fsc2c "$FreeSurferFolder"/surf/"$hemisphere"h.thickness "$FreeSurferFolder"/surf/"$hemisphere"h.white "$CaretFolder"/Native/temp.shape.gii
  caret_command -metric-math "$CaretFolder"/Native/temp.shape.gii "$CaretFolder"/Native/temp.shape.gii 1 "abs[@1@]"
  caret_command -metric-composite "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.shape.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.shape.gii "$CaretFolder"/Native/temp.shape.gii; mv "$CaretFolder"/Native/temp.shape.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".thickness.native.shape.gii
  caret_command -metric-composite-identified-columns "$CaretFolder"/Native/"$Subject"."$Hemisphere".thickness.native.metric "$CaretFolder"/Native/"$Subject"."$Hemisphere".thickness.native.shape.gii 1
  caret_command -file-convert -fsc2c "$FreeSurferFolder"/surf/"$hemisphere"h.curv "$FreeSurferFolder"/surf/"$hemisphere"h.white "$CaretFolder"/Native/"$Subject"."$Hemisphere".curvature.native.metric
  caret_command -surface-average "$CaretFolder"/Native/"$Subject"."$Hemisphere".midthickness.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".pial.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".white.native.coord.gii
  caret_command -spec-file-add "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec FIDUCIALcoord_file "$CaretFolder"/Native/"$Subject"."$Hemisphere".midthickness.native.coord.gii
  caret_command -spec-file-add "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec metric_file "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.metric
  caret_command -surface-generate-inflated "$CaretFolder"/Native/"$Subject"."$Hemisphere".midthickness.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii -iterations-scale 2.5 -generate-inflated -generate-very-inflated -output-spec "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec -output-inflated-file-name "$CaretFolder"/Native/"$Subject"."$Hemisphere".inflated.native.coord.gii -output-very-inflated-file-name "$CaretFolder"/Native/"$Subject"."$Hemisphere".very_inflated.native.coord.gii


  caret_command -deformation-map-create SPHERE "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".sphere.164k_fs.coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".FreeSurferReg.deform_map
  caret_command -deformation-map-create SPHERE "$CaretFolder"/Template/fsaverage."$Hemisphere".sphere.164k_fs.coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".FreeSurferRegInverse.deform_map
  cp "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.topo.gii
  DIR=`pwd`
  cd $CaretFolder
      caret_command -spec-file-create $Species $Subject $hemispherew OTHER -category Individual -spec-file-name "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec
  cd $DIR
  caret_command -spec-file-add "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec CLOSEDtopo_file "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.topo.gii
  caret_command -spec-file-add "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec volume_anatomy_file "$CaretFolder"/"$Subject".nii.gz
  i=1
  for Surface in white midthickness pial sphere sphere.reg ; do
    var=`echo "FIDUCIAL FIDUCIAL FIDUCIAL SPHERICAL SPHERICAL" | cut -d " " -f $i`
    caret_command -deformation-map-apply-generic-names "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".FreeSurferReg.deform_map COORDINATE "$CaretFolder"/Native/"$Subject"."$Hemisphere"."$Surface".native.coord.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere"."$Surface".164k_fs.coord.gii "$CaretFolder"/Native "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.native.coord.gii "." "." "$CaretFolder"/Template "$CaretFolder"/Template/fsaverage."$Hemisphere".template.164k_fs.spec "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".sphere.164k_fs.coord.gii "." "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec 
    caret_command -spec-file-add "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec "$var"coord_file "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere"."$Surface".164k_fs.coord.gii
    i=`echo "$i + 1" | bc`
  done
  caret_command -deformation-map-apply-generic-names "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".FreeSurferReg.deform_map SURFACE_SHAPE "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.shape.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.shape.gii "$CaretFolder"/Native "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.spec "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.native.coord.gii "." "." "$CaretFolder"/Template "$CaretFolder"/Template/fsaverage."$Hemisphere".template.164k_fs.spec "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".sphere.164k_fs.coord.gii "." "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec
  caret_command -spec-file-add "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec surface_shape_file "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.shape.gii
  caret_command -surface-generate-inflated "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".midthickness.164k_fs.coord.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.topo.gii -iterations-scale 2.5 -generate-inflated -generate-very-inflated -output-spec "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec -output-inflated-file-name "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".inflated.164k_fs.coord.gii -output-very-inflated-file-name "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".very_inflated.164k_fs.coord.gii

  cp "$AtlasFolder"/fsaverage."$Hemisphere".closed.164k_fs_LR.topo "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.topo.gii
  cp "$AtlasFolder"/fsaverage.LR.spherical_std.164k_fs_LR.coord "$CaretFolder"/"$Subject"."$Hemisphere".sphere.164k_fs_LR.coord.gii
  caret_command -surface-sphere "$CaretFolder"/"$Subject"."$Hemisphere".sphere.164k_fs_LR.coord.gii "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.topo.gii "$CaretFolder"/"$Subject"."$Hemisphere".sphere.164k_fs_LR.coord.gii
  cp "$AtlasFolder"/fsaverage."$Hemisphere".registered-to-fs_LR.164k_fs_LR.deform_map "$CaretFolder"/"$Subject"."$Hemisphere"2fsaverage_LR.164k_fs_LR.deform_map
  cp "$AtlasFolder"/FS_"$Hemisphere"/fsaverage.LR.registered-to-fs_"$Hemisphere".164k_fs_"$Hemisphere".deform_map "$CaretFolder"/fsaverage_LR.164k_fs_LR2"$Subject"."$Hemisphere".deform_map
  cp "$AtlasFolder"/FS_"$Hemisphere"/fs_"$Hemisphere"-to-fs_LR_fsaverage."$Hemisphere".spherical_std.164k_fs_"$Hemisphere".coord "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".def_sphere.164k_fs_"$Hemisphere".coord.gii
  caret_command -surface-sphere "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".def_sphere.164k_fs_"$Hemisphere".coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".def_sphere.164k_fs_"$Hemisphere".coord.gii
  cp "$AtlasFolder"/fsaverage."$Hemisphere".cut_cartesian-standard.164k_fs_LR.topo "$CaretFolder"/"$Subject"."$Hemisphere".cut_cartesian-standard.164k_fs_LR.topo.gii
  cp "$AtlasFolder"/fsaverage."$Hemisphere".cartesian-std.164k_fs_LR.coord "$CaretFolder"/"$Subject"."$Hemisphere".cartesian-std.164k_fs_LR.coord.gii
  DIR=`pwd`
  cd $CaretFolder
      caret_command -spec-file-create $Species $Subject $hemispherew OTHER -category Individual -spec-file-name "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec
  cd $DIR
  caret_command -spec-file-add "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec CUTtopo_file "$CaretFolder"/"$Subject"."$Hemisphere".cut_cartesian-standard.164k_fs_LR.topo.gii
  caret_command -spec-file-add "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec FLATcoord_file "$CaretFolder"/"$Subject"."$Hemisphere".cartesian-std.164k_fs_LR.coord.gii
  caret_command -spec-file-add "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec volume_anatomy_file "$CaretFolder"/"$Subject".nii.gz
  caret_command -spec-file-add "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec CLOSEDtopo_file "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.topo.gii
  caret_command -spec-file-add "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec SPHERICALcoord_file "$CaretFolder"/"$Subject"."$Hemisphere".sphere.164k_fs_LR.coord.gii
 
  i=1
  for Surface in white midthickness pial ; do
    var=`echo "FIDUCIAL FIDUCIAL FIDUCIAL" | cut -d " " -f $i`
    caret_command -deformation-map-apply-generic-names "$CaretFolder"/"$Subject"."$Hemisphere"2fsaverage_LR.164k_fs_LR.deform_map COORDINATE "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere"."$Surface".164k_fs.coord.gii "$CaretFolder"/"$Subject"."$Hemisphere"."$Surface".164k_fs_LR.coord.gii "$CaretFolder"/fsaverage "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.topo.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".sphere.164k_fs.coord.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".def_sphere.164k_fs_"$Hemisphere".coord.gii "." "." "$CaretFolder" "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.topo.gii "$CaretFolder"/"$Subject"."$Hemisphere".sphere.164k_fs_LR.coord.gii "." "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec
    caret_command -spec-file-add "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec "$var"coord_file "$CaretFolder"/"$Subject"."$Hemisphere"."$Surface".164k_fs_LR.coord.gii
    i=`echo "$i + 1" | bc`
  done
  caret_command -deformation-map-apply-generic-names "$CaretFolder"/"$Subject"."$Hemisphere"2fsaverage_LR.164k_fs_LR.deform_map SURFACE_SHAPE "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.shape.gii "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.shape.gii "$CaretFolder"/fsaverage "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.spec "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".164k_fs.topo.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".sphere.164k_fs.coord.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".def_sphere.164k_fs_"$Hemisphere".coord.gii "." "." "$CaretFolder" "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.topo.gii "$CaretFolder"/"$Subject"."$Hemisphere".sphere.164k_fs_LR.coord.gii "." "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec
  caret_command -spec-file-add "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec surface_shape_file "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.shape.gii
  caret_command -surface-generate-inflated "$CaretFolder"/"$Subject"."$Hemisphere".midthickness.164k_fs_LR.coord.gii "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.topo.gii -iterations-scale 2.5 -generate-inflated -generate-very-inflated -output-spec "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec -output-inflated-file-name "$CaretFolder"/"$Subject"."$Hemisphere".inflated.164k_fs_LR.coord.gii -output-very-inflated-file-name "$CaretFolder"/"$Subject"."$Hemisphere".very_inflated.164k_fs_LR.coord.gii

  DIR=`pwd`
  cd "$CaretFolder"
    caret_command -spec-file-change-resolution "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.spec "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k_"$Hemisphere" $DownSampleI
    caret_command -spec-file-add "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k_"$Hemisphere"/"$Subject"."$Hemisphere"."$DownSampleNameI"k_fs_LR.spec volume_anatomy_file "$CaretFolder"/"$Subject".nii.gz 
    rm "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k_"$Hemisphere"/def_sphere.coord
    caret_command -file-convert -format-convert ASCII "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k_"$Hemisphere"/def_sphere.deform_map
    cat "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k_"$Hemisphere"/def_sphere.deform_map | sed s@def_sphere.coord@"$Subject"."$Hemisphere".sphere."$DownSampleNameI"k_fs_LR.coord.gii@g | sed s@fsaverage_LR"$DownSampleNameI"k_"$Hemisphere"@fsaverage_LR"$DownSampleNameI"k@g > "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k_"$Hemisphere"/164k_fs_LR2"$DownSampleNameI"k_fs_LR."$Hemisphere".deform_map
    rm "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k_"$Hemisphere"/def_sphere.deform_map
    if [ ! -e "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k ] ; then
      mkdir "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k
    fi
    cp "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k_"$Hemisphere"/* "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k
    caret_command -deformation-map-create SPHERE "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k/"$Subject"."$Hemisphere".sphere."$DownSampleNameI"k_fs_LR.coord.gii "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k/"$Subject"."$Hemisphere"."$DownSampleNameI"k_fs_LR.topo.gii "$CaretFolder"/"$Subject"."$Hemisphere".sphere.164k_fs_LR.coord.gii "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.topo.gii "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k/"$DownSampleNameI"k_fs_LR2164k_fs_LR."$Hemisphere".deform_map
    rm -r "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k_"$Hemisphere"

  cd $DIR
  caret_command -surface-sphere-project-unproject "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.reg_LR.native.coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".sphere.164k_fs.coord.gii "$CaretFolder"/fsaverage/"$Subject"."$Hemisphere".def_sphere.164k_fs_"$Hemisphere".coord.gii "$CaretFolder"/Template/fsaverage."$Hemisphere".164k_fs.topo.gii
  caret_command -deformation-map-create SPHERE "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.reg_LR.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k/"$Subject"."$Hemisphere".sphere."$DownSampleNameI"k_fs_LR.coord.gii "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k/"$Subject"."$Hemisphere"."$DownSampleNameI"k_fs_LR.topo.gii "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k/native2"$DownSampleNameI"k_fs_LR."$Hemisphere".deform_map
  caret_command -deformation-map-create SPHERE "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k/"$Subject"."$Hemisphere".sphere."$DownSampleNameI"k_fs_LR.coord.gii "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k/"$Subject"."$Hemisphere"."$DownSampleNameI"k_fs_LR.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.reg_LR.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/fsaverage_LR"$DownSampleNameI"k/"$DownSampleNameI"k_fs_LR2native."$Hemisphere".deform_map
  caret_command -deformation-map-create SPHERE "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.reg_LR.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/"$Subject"."$Hemisphere".sphere.164k_fs_LR.coord.gii "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.topo.gii "$CaretFolder"/native2164k_fs_LR."$Hemisphere".deform_map
  caret_command -deformation-map-create SPHERE "$CaretFolder"/"$Subject"."$Hemisphere".sphere.164k_fs_LR.coord.gii "$CaretFolder"/"$Subject"."$Hemisphere".164k_fs_LR.topo.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".sphere.reg.reg_LR.native.coord.gii "$CaretFolder"/Native/"$Subject"."$Hemisphere".native.topo.gii "$CaretFolder"/164k_fs_LR2native."$Hemisphere".deform_map
done

