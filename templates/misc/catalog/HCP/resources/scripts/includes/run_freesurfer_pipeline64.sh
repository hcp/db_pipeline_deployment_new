#!/bin/bash

maxhours=48

server=$1
user=$2
infile=$3
subject=$4
workdir=$5
#subjectdir=$5

filename=`echo $infile | sed -s 's/^.*\///'`

#read -s -p "Password:  " passwd

##
## SEE IF PASSWORD IS REQUIRED FOR HOST
##

ssh -o "BatchMode yes" $user@$server whoami

rc=$?
if [ $rc -ne 0 ] ; then
   echo "ERROR:  Cannot SSH to remote machine without password"
   exit $rc
fi

##
## CREATE DIRECTORIES ON REMOTE SERVER
##

ssh $user@$server "mkdir -p ~/pipeline_runs/$subject/input;mkdir -p ~/pipeline_runs/$subject/script;mkdir -p /scratch/$user/$subject/FreeSurfer"

rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not create directory on remote server"
	exit $rc
fi

##
## COPY OVER INPUT FILE(S)
##

scp $infile $user@$server:~/pipeline_runs/$subject/input

rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not copy input file to remote server"
	exit $rc
fi

##
## CREATE FREESURFER SCRIPT FILE LOCALLY TO BE SENT TO REMOTE SERVER
##

#read -d '' FS_SCRIPT <<EOF
fsfile=`mktemp`
cat > $fsfile <<EOF
#!/bin/bash

# give the job a name to help keep track of running jobs (optional)
#PBS -N fs_subject

# Specify the resources needed
# We'll ask for 1 core of 1 node for 48 hours
#  PBS -l nodes=1:ppn=1:idataplex,walltime=48:00:00
#PBS -l nodes=1:ppn=1:idataplex,walltime=00:01:00

# Specify the default queue, not the SMP nodes
#PBS -q dque

## Supercomputer setup
#export FREESURFER_HOME=/export/freesurfer
#export PATH=\${FREESURFER_HOME}/bin:\${PATH}
#export PATH=\${FREESURFER_HOME}/mni/bin:\${PATH}
#export PERL5LIB=\${FREESURFER_HOME}/mni/lib/perl5/5.8.5/

## pipeline64.local setup
export FREESURFER_HOME=/usr/local/freesurfer
export PATH=\$FREESURFER_HOME/bin:\$PATH
export PATH=\$FREESURFER_HOME/mni/bin:\$PATH
source \$FREESURFER_HOME/SetUpFreeSurfer.sh

mkdir -p /scratch/$user/$subject/FreeSurfer 
ln -s ${FREESURFER_HOME}/subjects/fsaverage /scratch/$user/$subject/FreeSurfer/fsaverage 
time recon-all -i ~/pipeline_runs/$subject/input/$filename -subject $subject -sd /scratch/$user/$subject/FreeSurfer -autorecon1 -noskullstrip 
cd /scratch/$user/$subject/FreeSurfer/$subject/mri
cp -p T1.mgz brainmask.auto.mgz
cp -p T1.mgz brainmask.mgz
####time recon-all -i ~/pipeline_runs/$subject/input/$filename -subject $subject -sd /scratch/$user/$subject/FreeSurfer -autorecon2 -autorecon3 -qcache -force 
#time recon-all -i ~/pipeline_runs/$subject/input/$filename -subject $subject -sd /scratch/$user/$subject/FreeSurfer -autorecon2 -autorecon3 -force -dontrun 
#time recon-all -i ~/pipeline_runs/$subject/input/$filename -subject $subject -sd /scratch/$user/$subject/FreeSurfer -all 

EOF

rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Couldn't create freesurfer script file"
	exit $rc
fi

scp $fsfile $user@$server:~/pipeline_runs/$subject/script/fs_job.sh

rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not send FreeSurfer script to remote server"
	exit $rc
fi

##
## CLEAN-UP
##

rm $fsfile

###
### CREATE CLEANUP SCRIPT FILE LOCALLY TO BE SENT TO REMOTE SERVER
###
#
##read -d '' FS_SCRIPT <<EOF
#cleanupfile=`mktemp`
#cat > $cleanupfile <<EOF
##!/bin/bash
#
#echo HELLO WORLD!!!
#
#EOF
#
#if [ $? != 0 ] ; then
#	echo "ERROR:  Couldn't create local script file"
#	exit $?
#fi
#
###
### SEND CLEANUP SCRIPT FILE 
###
#
#scp $cleanupfile $user@$server:~/pipeline_runs/$subject/script/fs_cleanup.sh
#
#rc=$?
#if [ $rc != 0 ] ; then
#	echo "ERROR:  Could not send FreeSurfer script to remote server"
#	exit $rc
#fi
#
###
### CLEAN-UP
###
#
#rm $cleanupfile

##
## RUN FREESURFER PROCESS ON REMOTE SERVER
##

## RUN LOCALLY
pid=`ssh $user@$server "chmod u+x ~/pipeline_runs/$subject/script/fs_job.sh;nohup ~/pipeline_runs/$subject/script/fs_job.sh > ~/pipeline_runs/$subject/script/launch_fs_job.out 2> ~/pipeline_runs/$subject/script/launch_fs_job.err < /dev/null & echo \\$!"` 
echo PID=$pid
rc=$?
echo RC=$rc
### RUN ON SUPERCOMPUTER 
#ssh $user@$server "chmod u+x ~/pipeline_runs/$subject/script/fs_job.sh;nohup ~/pipeline_runs/$subject/script/fs_job.sh > ~/pipeline_runs/$subject/script/launch_fs_job.out &" 

###
### Check status hourly, send back when complete ##
###
#
finished=false
hour=1

while [ $hour -lt $maxhours ] && ! $finished ; do
   #sleep 3600
   sleep 60
   echo hour=$hour
   let hour+=1 
   words=`ssh $user@$server ps --no-headers -p $pid | wc -w`
   if [ $words -lt 1 ] ; then
      finished=true
   fi
done

if ! $finished ; then
	echo "ERROR:  Timed out waiting for FreeSurfer run to complete ($maxhours hours)"
	exit 1;
fi

echo "FINISHED - $hour"

##
## Retrieve Files
##
## 1) zip up output on remote server
ssh $user@$server "cd /scratch/$user/$subject/FreeSurfer;zip -r ../freesurfer.zip ." 
rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not zip up FreeSurfer output"
	exit $rc
fi
## 2) retrieve zip file
cd $workdir
scp $user@$server:/scratch/$user/$subject/freesurfer.zip .
rc=$?
if [ $rc != 0 ] ; then
	echo "ERROR:  Could not retrieve FreeSurfer output from remote server"
	exit $rc
fi
## 3) unzip the file
unzip freesurfer.zip 
rm freesurfer.zip
cd $workdir

##
## CLEAN-UP DIRECTORIES ON SERVER
##

## Comment this out for now.  Need FS on free surfer to run Matt's script
#ssh $user@$server "rm -r ~/pipeline_runs/$subject;rm -r /scratch/$user/$subject"


