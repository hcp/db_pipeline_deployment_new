#!/bin/tcsh


if ( ${#argv} < 7 ) then
  echo "Usage: phantom_qa  <REQUIRED full path to QA series XCEDE file> <QCAssessment File> <scan id> <roisize> <XNAT_ID> <QC_ID> <HOST>"
  exit -1;
endif

if (! -e $1) then
  echo "Error: native series XCEDE file $3 does not exist"
  exit -1
endif





set bxhOutputFile="$1:t"
set seriesFullPath="$1:h"
set seriesLabel="$seriesFullPath:t"

set qcAssessmentFile=$2
set scanId=$3
set roisize=$4
set xnatId=$5
set qcId=$6
set host=$7
set sessionLabel=$8
set project = $9

#source $SCRIPTS_HOME/bxh_xcede_tools_setup.csh

source @PIPELINE_DIR_PATH@/catalog/HCP_QC/BIRN/resources/bxh_xcede_tools-1.10.3_setup.csh

set HUMAN_QA = @PIPELINE_DIR_PATH@/catalog/HCP_QC/BIRN/resources/human_qa.csh

set bindir=$BXH_XCEDE_TOOLS_HOME/bin


echo "Working on directory\n$seriesFullPath"
echo "Working on series\n$seriesLabel"
echo "Working on native XCEDE file\n$bxhOutputFile"


#############################################################################
#  set up the output directory to Derived_Data/BIRN_QA
#
if( !(-d $seriesFullPath/BIRN_PHANTOM_QA/$scanId) ) then
  exit 1;
endif

set derivedDataQA="$seriesFullPath/BIRN_PHANTOM_QA/$scanId"
set humanderivedDataQA="$seriesFullPath/BIRN_HUMAN_QA/$scanId"

if( !(-d $derivedDataQA) ) then
  echo "Error,\ncould not generate output directory $derivedDataQA\n"
  exit -1;
endif

if( !(-d $humanderivedDataQA) ) then
  echo "Error,\ncould not generate output directory $humanderivedDataQA\n"
  exit -1;
endif

set today=`date +"%Y-%m-%d"`


echo $today


#############################################################################
#  output files - PHANTOM
#############################################################################

set outputHTMLFileName="data/archive/experiments/"$xnatId"/assessors/"$qcId"/out/resources/BIRN_AGAR_DATA/files/web_access_index.html"

set outputLogFileName="$derivedDataQA/stabilityQA.log"


#############################################################################
#  output files - HUMAN
#############################################################################

set outputXMLFileName="$humanderivedDataQA/qa_events_"$bxhOutputFile".xml"
set humanoutputHTMLFileName="data/archive/experiments/"$xnatId"/assessors/"$qcId"/out/resources/BIRN_DATA/files/web_access_index.html"


#############################################################################
#  run analysis script - PHANTOM
#############################################################################

set date=`date`
echo $bindir/fmriqa_phantomqa.pl "$seriesFullPath/$bxhOutputFile" --overwrite --roisize $roisize "$derivedDataQA" 

$bindir/fmriqa_phantomqa.pl "$seriesFullPath/$bxhOutputFile" --overwrite --roisize $roisize "$derivedDataQA"  >&! $outputLogFileName 


#############################################################################
#  run analysis script - HUMAN
#############################################################################

echo $bindir/fmriqa_generate.pl "$seriesFullPath/$bxhOutputFile" --overwrite --qalabel $sessionLabel":Scan"$scanId "$humanderivedDataQA"

$bindir/fmriqa_generate.pl "$seriesFullPath/$bxhOutputFile"  --overwrite --qalabel $sessionLabel":Scan"$scanId "$humanderivedDataQA" 




set mean_masked_fwhmx=`grep mean_masked_fwhmx $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_masked_fwhmy=`grep mean_masked_fwhmy $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_masked_fwhmz=`grep mean_masked_fwhmz $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_middle_slice=`grep mean_middle_slice $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_sfnr_middle_slice=`grep mean_sfnr_middle_slice $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_snr_middle_slice=`grep mean_snr_middle_slice $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`

pushd $humanderivedDataQA
	sed -e 's+src="\(.*\)"+src="'$host'data/archive/experiments/'$xnatId'/assessors/'$qcId'/out/resources/BIRN_DATA/files/\1"+' index.html > web_access_index_temp.html
	sed -e 's+href="index.css"+href="'$host'/style/birn/index.css"+' web_access_index_temp.html > web_access_index.html
	\rm -f web_access_index_temp.html
	
popd




pushd $derivedDataQA
	sed -e 's+src="\(.*\)"+src="'$host'data/archive/experiments/'$xnatId'/assessors/'$qcId'/out/resources/BIRN_AGAR_DATA/files/\1"+' index.html > web_access_index_temp.html
	sed -e 's+href="index.css"+src="'$host'/style/birn/index.css"+' web_access_index_temp.html > web_access_index.html
	\rm -f web_access_index_temp.html
popd


#############################################################################
#  retrieve data from OS and/or script output
#

set Mean=`grep "#mean=" $outputLogFileName | awk -F= '{print $2}'`
set SNR=`grep "#SNR=" $outputLogFileName | awk -F= '{print $2}'`
set SFNR=`grep "#SFNR=" $outputLogFileName | awk -F= '{print $2}'`
set StdDev=`grep "#std=" $outputLogFileName | awk -F= '{print $2}'`
set PercentFluctuation=`grep "#percentFluc=" $outputLogFileName | awk -F= '{print $2}'`
set Drift=`grep "#drift=" $outputLogFileName | awk -F= '{print $2}'`

set origdims=`grep "##orig. dimensions: " $outputLogFileName | sed 's/.*dimensions: //'`

set mincmassx=`grep "#mincmassx=" $outputLogFileName | awk -F= '{print $2}'`
set mincmassy=`grep "#mincmassy=" $outputLogFileName | awk -F= '{print $2}'`
set mincmassz=`grep "#mincmassz=" $outputLogFileName | awk -F= '{print $2}'`
set maxcmassx=`grep "#maxcmassx=" $outputLogFileName | awk -F= '{print $2}'`
set maxcmassy=`grep "#maxcmassy=" $outputLogFileName | awk -F= '{print $2}'`
set maxcmassz=`grep "#maxcmassz=" $outputLogFileName | awk -F= '{print $2}'`
set meancmassx=`grep "#meancmassx=" $outputLogFileName | awk -F= '{print $2}'`
set meancmassy=`grep "#meancmassy=" $outputLogFileName | awk -F= '{print $2}'`
set meancmassz=`grep "#meancmassz=" $outputLogFileName | awk -F= '{print $2}'`
set dispcmassx=`grep "#dispcmassx=" $outputLogFileName | awk -F= '{print $2}'`
set dispcmassy=`grep "#dispcmassy=" $outputLogFileName | awk -F= '{print $2}'`
set dispcmassz=`grep "#dispcmassz=" $outputLogFileName | awk -F= '{print $2}'`
set driftcmassx=`grep "#driftcmassx=" $outputLogFileName | awk -F= '{print $2}'`
set driftcmassy=`grep "#driftcmassy=" $outputLogFileName | awk -F= '{print $2}'`
set driftcmassz=`grep "#driftcmassz=" $outputLogFileName | awk -F= '{print $2}'`
set minfwhmx=`grep "#minfwhmx=" $outputLogFileName | awk -F= '{print $2}'`
set minfwhmy=`grep "#minfwhmy=" $outputLogFileName | awk -F= '{print $2}'`
set minfwhmz=`grep "#minfwhmz=" $outputLogFileName | awk -F= '{print $2}'`
set maxfwhmx=`grep "#maxfwhmx=" $outputLogFileName | awk -F= '{print $2}'`
set maxfwhmy=`grep "#maxfwhmy=" $outputLogFileName | awk -F= '{print $2}'`
set maxfwhmz=`grep "#maxfwhmz=" $outputLogFileName | awk -F= '{print $2}'`
set meanfwhmx=`grep "#meanfwhmx=" $outputLogFileName | awk -F= '{print $2}'`
set meanfwhmy=`grep "#meanfwhmy=" $outputLogFileName | awk -F= '{print $2}'`
set meanfwhmz=`grep "#meanfwhmz=" $outputLogFileName | awk -F= '{print $2}'`
set meanghost=`grep "#meanghost=" $outputLogFileName | awk -F= '{print $2}'`
set meanbrightghost=`grep "#meanbrightghost=" $outputLogFileName | awk -F= '{print $2}'`
set driftfit=`grep "#driftfit=" $outputLogFileName | awk -F= '{print $2}'`


set rdc=`grep "#rdc=" $outputLogFileName | awk -F= '{print $2}'`

#############################################################################
#  export script output to XNAT QCAssessment derived data xml file
#############################################################################

echo "<xnat:QCAssessment ID="\"$qcId\"" type="\""BIRN_AGAR_QC"\"" project="\""$project"\"" label="\"$qcId\"" xmlns:xnat="\""http://nrg.wustl.edu/xnat"\""  xmlns:xsi="\""http://www.w3.org/2001/XMLSchema-instance"\""  xsi:schemaLocation="\""http://nrg.wustl.edu/xnat https://cndabeta.wustl.edu/schemas/xnat/xnat.xsd"\"">" > $qcAssessmentFile

echo "<xnat:date>$today</xnat:date>" >> $qcAssessmentFile
echo "<xnat:imageSession_ID>$xnatId</xnat:imageSession_ID>"  >> $qcAssessmentFile

echo "<xnat:scans>" >> $qcAssessmentFile

echo '<xnat:scan id="'$scanId'">' >> $qcAssessmentFile
echo '<xnat:scanStatistics xsi:type="xnat:statisticsData">' >> $qcAssessmentFile


echo "<xnat:mean>${Mean}</xnat:mean>" >>  $qcAssessmentFile
echo "<xnat:snr>${SNR}</xnat:snr>" >>  $qcAssessmentFile
echo "<xnat:stddev>${StdDev}</xnat:stddev>" >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""SFNR"\"">${SFNR}</xnat:additionalStatistics>" >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""PercentFluctuation"\"">${PercentFluctuation}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""Drift"\"">${Drift}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""rdc"\"">${rdc}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""mincmassx"\"">${mincmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""mincmassy"\"">${mincmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""mincmassz"\"">${mincmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""maxcmassx"\"">${maxcmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""maxcmassy"\"">${maxcmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""maxcmassz"\"">${maxcmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""meancmassx"\"">${meancmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meancmassy"\"">${meancmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meancmassz"\"">${meancmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""dispcmassx"\"">${dispcmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""dispcmassy"\"">${dispcmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""dispcmassz"\"">${dispcmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""driftcmassx"\"">${driftcmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""driftcmassy"\"">${driftcmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""driftcmassz"\"">${driftcmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""minfwhmx"\"">${minfwhmx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""minfwhmy"\"">${minfwhmy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""minfwhmz"\"">${minfwhmz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""maxfwhmx"\"">${maxfwhmx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""maxfwhmy"\"">${maxfwhmy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""maxfwhmz"\"">${maxfwhmz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""meanfwhmx"\"">${meanfwhmx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meanfwhmy"\"">${meanfwhmy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meanfwhmz"\"">${meanfwhmz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""meanghost"\"">${meanghost}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meanbrightghost"\"">${meanbrightghost}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""driftfit"\"">${driftfit}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""BIRN_HUMAN_MEAN"\"">${mean_middle_slice}</xnat:additionalStatistics>" >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""BIRN_HUMAN_SNR"\"">${mean_snr_middle_slice}</xnat:additionalStatistics>" >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""BIRN_HUMAN_SFNR"\"">${mean_sfnr_middle_slice}</xnat:additionalStatistics>" >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""BIRN_HUMAN_MEAN_MASKED_FWHMX"\"">${mean_masked_fwhmx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""BIRN_HUMAN_MEAN_MASKED_FWHMY"\"">${mean_masked_fwhmy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""BIRN_HUMAN_MEAN_MASKED_FWHMZ"\"">${mean_masked_fwhmz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:addField name="\""BIRN_HUMAN_HTML"\"">$humanoutputHTMLFileName</xnat:addField>" >> $qcAssessmentFile
echo "<xnat:addField name="\""BIRN_AGAR_HTML"\"">$outputHTMLFileName</xnat:addField>" >> $qcAssessmentFile

echo '</xnat:scanStatistics>' >> $qcAssessmentFile

echo '</xnat:scan>' >> $qcAssessmentFile

echo "</xnat:scans>" >> $qcAssessmentFile
echo "</xnat:QCAssessment>" >> $qcAssessmentFile

exit 0
