#!/bin/csh

#This is a wrapper script to generate the QCAssessment element for the Phantom QA
#using the BIRN Phantom QA techinique
#Refer: https://xwiki.nbirn.net/xwiki/bin/view/Function-BIRN/AutomatedQA

if ( ${#argv} < 3 ) then
  echo "Usage: phantom_qa_wrapper <XNAT ID>  <Project Id> <Path to the QCAssessment File>"
  exit -1;
endif

set xnatId=$argv[1]
set project=$argv[2]
set sessionLabel=$argv[3]
set subject=$argv[4]
set roisize=$argv[5]
set host=$argv[6]
set user = $argv[7]
set pass = $argv[8]


set siteId = "1"
set siteName = "HCPIntraDB"


set XNATRestClient = @PIPELINE_DIR_PATH@/xnat-tools/XNATRestClient


set wrkdir = $cwd

foreach s (`ls -d */ | xargs -l basename`)
	set imagefile = $wrkdir/$s"_image.xml"
	set scanId = `echo $s | sed "s/scan//"`
	set date = `date "+%Y%m%d%H%M"`
	set qcAssessmentFile = $wrkdir/BIRN_AGAR_QC_$scanId.xml

	set qc_id = $xnatId"_SCAN${scanId}_BIRN_AGAR_QC"_$date
	
	mkdir -p $wrkdir/BIRN_PHANTOM_QA/$scanId
	mkdir -p $wrkdir/BIRN_HUMAN_QA/$scanId
	
	@PIPELINE_DIR_PATH@/catalog/HCP_QC/BIRN/resources/phantom_qa.csh $imagefile $qcAssessmentFile $scanId $roisize $xnatId $qc_id $host $sessionLabel $project
	if ($status) exit 1
	
	################################
	# Upload the XML
	#
	################################
	set restPath = data/archive/projects/${project}/subjects/${subject}/experiments/${sessionLabel}/assessors/${qc_id}
	echo $restPath

	$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath" -local $qcAssessmentFile

	################################
	# Upload the various files
	#
	################################


	pushd $wrkdir/BIRN_PHANTOM_QA/$scanId

	zip -r phantom_$scanId *

	$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/out/resources/BIRN_AGAR_DATA" 

	$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/resources/BIRN_AGAR_DATA/files?extract=true\&label=BIRN_AGAR_DATA\&content=BIRN_AGAR_DATA" -local phantom_${scanId}.zip

	popd

	pushd $wrkdir/BIRN_HUMAN_QA/$scanId

	zip -r human_$scanId *

	$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/out/resources/BIRN_DATA" 

	$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/resources/BIRN_DATA/files?extract=true\&label=BIRN_AGAR_DATA\&content=BIRN_DATA" -local human_${scanId}.zip

	popd


end


exit 0
