#!/bin/tcsh


if ( ${#argv} < 7 ) then
  echo "Usage: human_qa <XNAT ID> <QC ID> <REQUIRED full path to QA series XCEDE file> <QCAssessment File> <scan id> <sessionLabel> <Outdir>"
  exit -1;
endif

if (! -e $3) then
  echo "Error: native series XCEDE file $3 does not exist"
  exit -1
endif

if (! -e $4) then
  echo "Error: QCAssessment File doesnt exist. Cannt append to it"
  exit -1
endif




set xnatId="$1"
set qcId="$2"
set bxhOutputFile="$3:t"
set seriesFullPath="$3:h"
set seriesLabel="$seriesFullPath:t"

set qcAssessmentFile=$4
set scanId=$5
set sessionLabel=$6
set outdir=$7

source @PIPELINE_DIR_PATH@/catalog/HCP_QC/BIRN/resources/bxh_xcede_tools-1.10.3_setup.csh

set bindir=$BXH_XCEDE_TOOLS_HOME/bin



echo "Working on directory\n$seriesFullPath"
echo "Working on series\n$seriesLabel"
echo "Working on native XCEDE file\n$bxhOutputFile"


#############################################################################
#  set up the output directory to Derived_Data/BIRN_QA
#############################################################################

if( !(-d $outdir/$scanId) ) then
  exit 1;
endif

set derivedDataQA="$outdir/$scanId"
if( !(-d $derivedDataQA) ) then
  echo "Error,\ncould not generate output directory $derivedDataQA\n"
  exit -1;
endif


#############################################################################
#  output files
#############################################################################

set outputXMLFileName="$derivedDataQA/qa_events_"$bxhOutputFile".xml"
set outputHTMLFileName="data/archive/experiments/"$xnatId"/assessors/"$qcId"/out/resources/BIRN_DATA/files/web_access_index.html"



#############################################################################
#  run analysis script
#
set date=`date`
echo $bindir/fmriqa_generate.pl "$seriesFullPath/$bxhOutputFile" --overwrite --qalabel $sessionLabel":Scan"$scanId "$derivedDataQA"

$bindir/fmriqa_generate.pl "$seriesFullPath/$bxhOutputFile"  --overwrite --qalabel $sessionLabel":Scan"$scanId "$derivedDataQA" 


set mean_masked_fwhmx=`grep mean_masked_fwhmx $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_masked_fwhmy=`grep mean_masked_fwhmy $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_masked_fwhmz=`grep mean_masked_fwhmz $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_middle_slice=`grep mean_middle_slice $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_sfnr_middle_slice=`grep mean_sfnr_middle_slice $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
set mean_snr_middle_slice=`grep mean_snr_middle_slice $outputXMLFileName | awk -F\> '{print $2}' | awk -F\< '{print $1}'`



#############################################################################
#  export script output to XNAT QCAssessment derived data xml file
#


echo "<xnat:additionalStatistics name="\""BIRN_MEAN"\"">${mean_middle_slice}</xnat:additionalStatistics>" >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""BIRN_SNR"\"">${mean_snr_middle_slice}</xnat:additionalStatistics>" >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""BIRN_SFNR"\"">${mean_sfnr_middle_slice}</xnat:additionalStatistics>" >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""BIRN_MEAN_MASKED_FWHMX"\"">${mean_masked_fwhmx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""BIRN_MEAN_MASKED_FWHMY"\"">${mean_masked_fwhmy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""BIRN_MEAN_MASKED_FWHMZ"\"">${mean_masked_fwhmz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:addField name="\""BIRN_HTML"\"">$outputHTMLFileName</xnat:addField>" >> $qcAssessmentFile


exit 0
