#!/bin/bash -x
set -e

####
#
# This script will launch the pipeline on CHPC cluster or the NRG cluster
# The default behaviour is to launch it on NRG unless the pipeline has a parameter
# compute_cluster=CHPC
#
####


get_opt1() {
    arg=`echo $1 | sed 's/=.*//'`
    echo $arg
}


get_arg1() {
    if [ X`echo $1 | grep '='` = X ] ; then 
	echo "Option $1 requires an argument" 1>&2
	exit 1
    else 
	arg=`echo $1 | sed 's/.*=//'`
	if [ X$arg = X ] ; then
	    echo "Option $1 requires an argument" 1>&2
	    exit 1
	fi
	echo $arg
    fi
}

get_arg_name() {
    if [ X`echo $1 | grep '='` = X ] ; then 
	echo "Option $1 requires an argument" 1>&2
	exit 1
    else 
	name=`echo $1 | awk '{split($0,a,"="); print a[1]}'`
	if [ X$name = X ] ; then
	    echo "Option $1 requires an argument" 1>&2
	    exit 1
	fi
	echo $name
    fi
}

get_arg_value() {
    if [ X`echo $1 | grep '='` = X ] ; then 
	echo "Option $1 requires an argument" 1>&2
	exit 1
    else 
	value=`echo $1 | awk '{split($0,a,"="); print a[2]}'`
	if [ X$value = X ] ; then
	    echo "Option $1 requires an argument" 1>&2
	    exit 1
	fi
	echo $value
    fi
}


get_arg2() {
    if [ X$2 = X ] ; then
	echo "Option $1 requires an argument" 1>&2
	exit 1
    fi
    echo $2
}

create_job_file_prolog() {
  job_file=$1	
  touch $job_file
  echo "#PBS -l nodes=1:ppn=1,walltime=24:00:00,vmem=30000mb" > $job_file
  echo "#PBS -q dque" >> $job_file
  echo "#PBS -o $CHPC_LOG_FILE_PATH" >> $job_file
  echo "#PBS -e $CHPC_LOG_FILE_PATH" >> $job_file
  echo "echo \"Job started on \`hostname\` at \`date\`\"" >> $job_file
}



create_job_file_epilog() {
  job_file=$1	
  echo "echo \" \"" >> $job_file
  echo "echo \"Job Ended at \`date\`\""  >> $job_file
  echo "echo \" \"" >> $job_file

}

log_job_launch() {
	launch_time=`date`
	echo "------------------------------------------" >> $log_file
	echo $launch_time >> $log_file
	echo "------------------------------------------" >> $log_file
}


source "@PIPELINE_DIR_PATH@"/config/schedule.config

xnatpipeline_launcher=$1

shift;

builddir_path=
pipeline=
project=
parameterFile=
compute_cluster=

declare -i found
found=0
other_args=

while [ $found -lt 5 ] ; do
    if [ $# -eq 0 ] ; then
      break;
    fi
    iarg=`get_opt1 $1`;	
    case "$iarg"
	in
	-project)
	    project=`get_arg2 $1 $2`;
	    found=$found+1;
	    shift 2
	    ;;
	-pipeline)
	    pipeline=`get_arg2 $1 $2`;
	    found=$found+1;
	    shift 2;;
	-parameterFile)
	    parameterFile=`get_arg2 $1 $2`;
	    found=$found+1;
	    shift 2
	    ;;
	-parameter)
	    name_value_pair=`get_arg2 $1 $2`;
	    param_name=`get_arg_name $name_value_pair`;
	    param_value=`get_arg_value $name_value_pair`;
	    if [ $param_name = builddir ] ; then
	       builddir_path=$param_value	
	       found=$found+1;
	    elif [ $param_name = compute_cluster ] ; then
	       compute_cluster=$param_value	
	       found=$found+1;
	    else 
	      other_arguments=${other_arguments}" "$1" "$2		    
	    fi
	    shift 2
	    ;;
	-h)
	    usage;
	    exit 0;;
	*) 
	   other_arguments=${other_arguments}" "$1
	   shift
	   if [ $# -eq 0 ] ; then
	     found=5
	   fi
	   ;;	
    esac
done


builddir_root=`echo $builddir_path | awk -F"/${project}/" '{print $1}'`
builddir_datestamp=`echo $builddir_path | awk -F"/${project}/" '{print $2}'


cluster_name=$compute_cluster

if [ X$cluster_name = X ] ; then
 #No cluster specified. Look for cluster specification in the parameter file 
 if [ X$parameterFile != X ] ; then
   cluster_name=`grep -A 2 compute_cluster $parameterFile | grep pip:unique | awk '{split($0,a,">"); print a[2]}' | awk '{split($0,a,"<"); print a[1]}'` 
 fi
fi

if [ X$cluster_name = X ] ; then
 #Default to NRG
 #$launch_cmd
 echo "Launching pipeline on NRG cluster" 	
 log_job_launch
 echo $launch_cmd >> $log_file
 echo "-------------------------------------------" >> $log_file
else
  #extract cluster name
  if [ $cluster_name = CHPC ] ; then               
    chpc_job_file=${builddir_path}/chpc_${builddir_datestamp}.sh
    create_job_file_prolog $chpc_job_file
    chpc_build_path=$CHPC_XNAT_BUILD_PATH/$project/$builddir_datestamp
    chpc_pipeline_path=`echo $pipeline | sed "s&^$PIPELINE_HOME&\$PIPELINE_HOME&"`
    echo $chpc_pipeline_path
    remote_chpc_job_file=$chpc_build_path/chpc_${builddir_datestamp}.sh 	   
 
    launch_cmd="\$PIPELINE_HOME/bin/XnatPipelineLauncher -project $project -pipeline $chpc_pipeline_path -parameter builddir=$chpc_build_path -parameter compute_cluster=$cluster_name $other_arguments "
    if [ X$parameterFile != X ] ; then
	trailing_paramfile_path=`echo $parameterFile | sed "s&^$builddir_path&&"`
  	launch_cmd=${launch_cmd}" -parameterFile $chpc_build_path$trailing_paramfile_path "
    fi

    echo " $launch_cmd " >> $chpc_job_file	
    echo $lunch_cmd
    exit 0;
    create_job_file_epilog $chpc_job_file
    pushd ${builddir_root}/${project}
       scp -i $SSH_IDENTITY_FILE -r $builddir_datestamp ${CHPC_PIPELINE_USER}@${CHPC_LOGIN_NODE}:${CHPC_XNAT_BUILD_PATH}/$project/
    popd

    job_id=`ssh -i $SSH_IDENTITY_FILE -t ${CHPC_PIPELINE_USER}@${CHPC_LOGIN_NODE} "qsub -V $remote_chpc_job_file"`
    log_job_launch 	
    echo $launch_cmd >> $log_file
    echo "CHPC_JOB_ID=$job_id" >> $log_file
    echo "-------------------------------------------" >> $log_file
  fi
fi

exit 0;

